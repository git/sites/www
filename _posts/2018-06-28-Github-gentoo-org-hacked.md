---
title: 'Github Gentoo organization hacked - resolved'
---

## 2018-07-04 14:00 UTC
We believe this incident is now resolved. Please see the [incident report](https://wiki.gentoo.org/wiki/Github/2018-06-28 "Incident Report") for details about the incident, its impact, and resolution.

## 2018-06-29 15:15 UTC
The community raised questions about the provenance of Gentoo packages. Gentoo development is performed on
hardware run by the Gentoo Infrastructure team (not `github`). The Gentoo hardware was unaffected by this incident.
Users using the default Gentoo mirroring infrastructure should not be affected.

If you are still concerned about provenance or are unsure what solution you are using, please consult https://wiki.gentoo.org/wiki/Project:Portage/Repository_Verification. This will instruct you on how to verify your repository.

## 2018-06-29 06:45 UTC
The `gentoo` GitHub organization remains temporarily locked down by GitHub
support, pending fixes to pull-request content.

For ongoing status, please see the [Gentoo infra-status incident page](https://infra-status.gentoo.org/notice/20180629-github).

For later followup, please see the Gentoo Wiki page for [GitHub 2018-06-28](https://wiki.gentoo.org/wiki/Github/2018-06-28). An incident post-mortem will follow on the wiki.
