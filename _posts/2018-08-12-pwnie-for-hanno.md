---
title: 'Congratulations: Hanno Böck and co-authors win Pwnie!'
---

<a href="https://pwnies.com/" class="news-img-right">
  <img src="/assets/img/news/2018/logo-pwnies.png" alt="Pwnies logo">
</a>

Congratulations to security researcher and Gentoo developer [Hanno Böck](https://wiki.gentoo.org/wiki/User:Hanno) and his co-authors Juraj Somorovsky and Craig Young for winning one of this year's coveted [Pwnie awards](https://pwnies.com/winners/#crypto)!

The award is for their work on the [Return Of Bleichenbacher's Oracle Threat or ROBOT vulnerability](https://robotattack.org/), which at the time of discovery affected such illustrious sites as Facebook and Paypal. Technical details can be found in the [full paper published at the Cryptology ePrint Archive](https://eprint.iacr.org/2017/1189).
