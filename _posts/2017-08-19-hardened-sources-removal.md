---
title: 'Hardened Linux kernel sources removal'
---

As you may know the core of 
[sys-kernel/hardened-sources](https://packages.gentoo.org/packages/sys-kernel/hardened-sources) 
has been the grsecurity patches. Recently the grsecurity developers have
[decided to limit access to these patches](https://grsecurity.net/passing_the_baton.php). 
As a result, the Gentoo Hardened team is unable to ensure a regular 
patching schedule and therefore the security of the users of these 
kernel sources. Thus, we will be masking hardened-sources on the 27th of 
August and will proceed to remove them from the main ebuild repository by the end of 
September. We recommend to use
[sys-kernel/gentoo-sources](https://packages.gentoo.org/packages/sys-kernel/gentoo-sources) 
instead. Userspace hardening and support for SELinux will of course 
remain in the Gentoo ebuild repository. Please see 
[the full news item](https://www.gentoo.org/support/news-items/2017-08-19-hardened-sources-removal.html)
for additional information and links.
