---
title: 'Bootable Gentoo QCOW2 disk images - ready for the cloud!'
---

<img align="right" height="125" style="margin-left: 30px;" src="https://www.gentoo.org/assets/img/news/2025/larry-qcow2.png" alt="Larry the Qcow2">
We are very happy to announce <a href="https://www.gentoo.org/downloads/">new official 
downloads</a> on our website and our mirrors: Gentoo for amd64 (x86-64) and arm64 (aarch64), 
as immediately bootable disk images in qemu's QCOW2 format! The images, updated weekly, 
include an EFI boot partition and a fully functional Gentoo installation; either with no 
network activated but a password-less root login on the console ("no root pw"), or with 
network activated, all accounts initially locked, but 
<a href="https://cloudinit.readthedocs.io/en/latest/">cloud-init</a> running on boot 
("cloud-init"). Enjoy, and 
<a href="https://www.gentoo.org/news/2025/02/20/gentoo-qcow2-images.html">read on for more</a>!

<!--more-->

## Questions and answers

### How can I quickly test the images?

We recommend using the "no root password" images and qemu system emulation. Both amd64 and arm64 
images have all the necessary drivers ready for that. Boot them up, use as login name "root", 
and you will immediately get a fully functional Gentoo shell. The set of installed packages
is similar to that of an administration or rescue system, with a focus more on network
environment and less on exotic hardware. Of course you can emerge whatever you need though,
and binary package sources are already configured too.

### What settings do I need for qemu?

You need qemu with the target architecture (aarch64 or x86_64) enabled in QEMU_SOFTMMU_TARGETS,
and the UEFI firmware. 
<pre>
app-emulation/qemu
sys-firmware/edk2-bin
</pre>
You should disable the useflag "pin-upstream-blobs" on qemu and update edk2-bin at least to the
2024 version. Also, since you probably want to use KVM hardware acceleration for the virtualization, 
make sure that your kernel supports that and that your current user is in the kvm group.

For testing the amd64 (x86-64) images, a command line could look like this, configuring
8G RAM and 4 CPU threads with KVM acceleration:
<pre>
qemu-system-x86_64 \
        -m 8G -smp 4 -cpu host -accel kvm -vga virtio -smbios type=0,uefi=on \
        -drive if=pflash,unit=0,readonly=on,file=/usr/share/edk2/OvmfX64/OVMF_CODE_4M.qcow2,format=qcow2 \
        -drive file=di-amd64-console.qcow2 &
</pre>

For testing the arm64 (aarch64) images, a command line could look like this:
<pre>
qemu-system-aarch64 \
        -machine virt -cpu neoverse-v1 -m 8G -smp 4 -device virtio-gpu-pci -device usb-ehci -device usb-kbd \
        -drive if=pflash,unit=0,readonly=on,file=/usr/share/edk2/ArmVirtQemu-AARCH64/QEMU_EFI.qcow2 \
        -drive file=di-arm64-console.qcow2 &
</pre>

Please consult the <a href="https://www.qemu.org/docs/master/system/index.html">qemu documentation</a> 
for more details.

### Can I install the images onto a real harddisk / SSD?

Sure. Gentoo can do anything. The limitations are:
 * you need a disk with sector size 512 bytes (otherwise the partition table of the image file will not work), see the "SSZ" value in the following example:
<pre>
pinacolada ~ # blockdev --report /dev/sdb
RO    RA   SSZ   BSZ        StartSec            Size   Device
rw   256   512  4096               0   4000787030016   /dev/sdb
</pre>

 * your machine must be able to boot via UEFI (no legacy boot)
 * you may have to adapt the configuration yourself to disks, hardware, ...

So, <b>this is an expert workflow</b>.

Assuming your disk is /dev/sdb and has a size of at least 20GByte, you can then use the utility 
<a href="https://www.qemu.org/docs/master/tools/qemu-img.html">qemu-img</a>
to decompress the image onto the raw device. Warning, this obviously <b>overwrites the first 20Gbyte of /dev/sdb</b>
(and with that the existing boot sector and partition table):
<pre>
qemu-img convert -O raw di-amd64-console.qcow2 /dev/sdb
</pre>
Afterwards, you can and should extend the new root partition with xfs_growfs, create an additional swap partition behind it,
possibly adapt /etc/fstab and the grub configuration, ...

If you are familiar with partitioning and handling disk images you can for sure imagine
more workflow variants; you might find also the 
<a href="https://www.qemu.org/docs/master/tools/qemu-nbd.html">qemu-nbd</a> tool interesting.

### So what are the cloud-init images good for?

Well, for the cloud. Or more precisely, for any environment where a configuration data source
for <a href="https://cloudinit.readthedocs.io/en/latest/">cloud-init</a> is available. If this 
is already provided for you, the image should work out of the box. If not, well, you can 
<a href="https://cloudinit.readthedocs.io/en/latest/reference/datasources/nocloud.html">provide
the configuration data manually</a>, but be warned that this is a non-trivial task.

### Are you planning to support further architectures?

Eventually yes, in particular (EFI) riscv64 and loongarch64.

### Are you planning to support legacy boot?

No, since the placement of the bootloader outside the file system complicates things.

### How about disks with 4096 byte sectors?

Well... let's see how much demand this feature finds. If enough people are interested, we should
be able to generate an alternative image with a corresponding partition table.

### Why XFS as file system?

It has some features that ext4 is sorely missing (reflinks and copy-on-write), but at the
same time is rock-solid and reliable.
