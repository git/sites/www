---
title: 'DTrace 2.0 for Gentoo'
---

<a href="https://github.com/oracle/dtrace-utils/wiki" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2024/dtrace-ponycorn.png" alt="The DTrace Ponycorn"/>
</a>

The real, mythical <a href="https://github.com/oracle/dtrace-utils/wiki">DTrace</a> comes to Gentoo! Need to dynamically trace
your kernel or userspace programs, with rainbows, ponies, and unicorns - and all entirely safely and in production?!
Gentoo is now ready for that! Just emerge
<a href="https://packages.gentoo.org/packages/dev-debug/dtrace"><tt>dev-debug/dtrace</tt></a>
and you're all set. All required kernel options are already enabled in the newest stable Gentoo distribution kernel; if you
are compiling manually, the DTrace ebuild will inform you about required configuration changes.
Internally, <a href="https://github.com/oracle/dtrace-utils">DTrace 2.0 for Linux</a> builds on the BPF
engine of the Linux kernel, so don't be surprised if the awesome cross-compilation features of Gentoo are
used to install a gcc that outputs BPF code (which, btw, also comes in very handy for 
<a href="https://packages.gentoo.org/packages/sys-apps/systemd"><tt>sys-apps/systemd</tt></a>).

Documentation? Sure, there's lots of it. You can start with our <a href="https://wiki.gentoo.org/wiki/DTrace">DTrace
wiki page</a>, the <a href="https://github.com/oracle/dtrace-utils">DTrace for Linux page on GitHub</a>,
or the original <a href="https://illumos.org/books/dtrace/preface.html#preface">documentation for Illumos</a>.
Enjoy!
