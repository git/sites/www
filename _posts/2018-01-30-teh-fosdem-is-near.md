---
title: 'FOSDEM is near'
---
 <!-- Yes, I almost went with "Teh FOSDEM nearth", but decided against it since 'nearth' is not a real word. Nevermind the 'Teh', mellenials love using 'teh'. Gentoo can be fun! =) -->

Excitement is building with FOSDEM 2018 only a few days away. There are now [14 current](https://wiki.gentoo.org/wiki/FOSDEM_2018#Developers) and [one former](https://wiki.gentoo.org/wiki/User:Dberkholz) developer in planned attendance, along with many from the Gentoo community.

This year one Gentoo related talk titled *[Unix? Windows? Gentoo!](https://fosdem.org/2018/schedule/event/unix_windows_gentoo/)* will be given by [Michael Haubenwallner](https://fosdem.org/2018/schedule/speaker/michael_haubenwallner/) ([haubi <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:Haubi)) of the [Gentoo Prefix](https://wiki.gentoo.org/wiki/Project:Prefix) project.

Two more non-Gentoo related talks will be given by current Gentoo developers:

* [Andreas K. Huettel](https://fosdem.org/2018/schedule/speaker/andreas_k_huettel/) ([dilfridge <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:Dilfridge)) – *[Perl in the Physics Lab](https://fosdem.org/2018/schedule/event/perl_physics/)*
* [Andrew Savchenko](https://fosdem.org/2018/schedule/speaker/andrew_savchenko/) ([bircoph <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:Bircoph)) – *[The Invisible Internet Project](https://fosdem.org/2018/schedule/event/i2p/)*

If you attend don't miss out on your opportunity to build the Gentoo web-of-trust by bringing a valid governmental ID, a printed key fingerprint, and various UIDs you want certified. See you at FOSDEM!
