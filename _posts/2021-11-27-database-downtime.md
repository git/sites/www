---
title: 'Unexpected database server downtime, affecting bugs, forums, wiki'
---

<a href="https://www.gentoo.org/downloads/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/logo/gentoo-signet.svg" alt="Gentoo logo" width="80"/>
</a>

Due to an unexpected breakage on our database servers, several Gentoo websites are currently down.
In particular, this includes Forums, Wiki, and Bugzilla. Please visit our [Infrastructure status
page](https://infra-status.gentoo.org/) for real-time monitoring and eventual outage notices.

<b>Update, Dec 5, 2021:</b> The outage, apparently caused by a subtle bug in the depths of MariaDB
and Galera, should mostly be over. Our infrastructure team is watching the database servers closely
though.
