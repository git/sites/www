---
title: 'Gentoo on Android 64-bit release'
---

<a href="https://wiki.gentoo.org/wiki/Project:Android/tarball"><img src="https://www.gentoo.org/assets/img/news/2020/logo-gentoo-android.png" alt="gentoo-android logo" class="news-img-right"></a>

Gentoo [Project Android](https://wiki.gentoo.org/wiki/Project:Android) is pleased to announce a new 64-bit release of the [stage3 Android prefix tarball](https://wiki.gentoo.org/wiki/Project:Android/tarball).
This is a major release after 2.5 years of development, featuring gcc-10.1.0, binutils-2.34 and glibc-2.31. Enjoy Gentoo in your pocket!