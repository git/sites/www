---
title: 'In Memory of Kent “kentnl” Fredric'
---

Gentoo mourns the sudden loss of Kent Fredric, also known to us by his IRC 
handle kent\n. He passed away following a tragic accident a few days ago. 

Kent was an active member of the Gentoo community for many years. He 
tirelessly managed Gentoo's Perl support, and was active in the Rust project 
as well as in many other corners. We all remember him as an enthusiastic, 
bright person, with lots of eye for detail and constant willingness to help 
out and improve things.
On behalf of the world-wide Gentoo community, our heartfelt condolences go out 
to his family and friends.

Please join us in remembering Kent [on the Gentoo forums](https://forums.gentoo.org/viewtopic-t-1130094.html).
