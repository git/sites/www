---
title: 'Gentoo goes Binary!'
---

<a href="https://www.gentoo.org/news/2023/12/29/Gentoo-binary.html" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2023/larry-packages.png" alt="Larry the cow with packages"/>
</a>

You probably all know Gentoo Linux as your favourite source-based distribution.
Did you know that our package manager, Portage, already for years also has support for binary
packages, and that source- and binary-based package installations can be freely mixed?

To speed up working with slow hardware and for overall convenience, we're now also offering
**binary packages for download and direct installation**! For most architectures, this is
limited to the core system and weekly updates - not so for **amd64 and arm64** however. There we've got a 
stunning **>20 GByte of packages** on our mirrors, from LibreOffice to KDE Plasma and from Gnome to Docker.
Gentoo stable, updated daily. Enjoy! And <a href="https://www.gentoo.org/news/2023/12/29/Gentoo-binary.html">read 
on for more details!</a>

<!--more-->

## Questions & Answers

### How can I set up my existing Gentoo installation to use these packages?

[Quick setup instructions](https://wiki.gentoo.org/wiki/Binary_package_quickstart) for
the most common cases can be found in our wiki. In short, you need to create a configuration 
file in `/etc/portage/binrepos.conf/`.

In addition, we have a rather neat [binary package guide](https://wiki.gentoo.org/wiki/Binary_package_guide) on
our Wiki that goes into much more detail. 

### What do I have to do with a new stage / new installation?

New stages already contain the suitable `/etc/portage/binrepos.conf/gentoobinhost.conf`. You are
good to go from the start, although you may want to replace the `src-uri` 
setting in there with an URI pointing to the corresponding directory on a 
[local mirror](https://www.gentoo.org/downloads/mirrors/).

> `$ emerge -uDNavg @world`

### What compile settings, use flags, ... do the ''normal'' amd64 packages use?

The binary packages under `amd64/binpackages/17.1/x86-64` are compiled using
`CFLAGS="-march=x86-64 -mtune=generic -O2 -pipe"` and will work with any amd64 / x86-64 machine.

The available useflag settings and versions correspond to the stable packages
of the `amd64/17.1/nomultilib` (i.e., openrc), `amd64/17.1/desktop/plasma/systemd`,
and `amd64/17.1/desktop/gnome/systemd` profiles. This should provide fairly large
coverage.

### What compile settings, use flags, ... do the ''normal'' arm64 packages use?

The binary packages under `arm64/binpackages/17.0/arm64` are compiled using
`CFLAGS="-O2 -pipe"` and will work with any arm64 / AArch64 machine.

The available useflag settings and versions correspond to the stable packages
of the `arm64/17.0` (i.e., openrc), `arm64/17.0/desktop/plasma/systemd`,
and `arm64/17.0/desktop/gnome/systemd` profiles.

### But hey, that's not optimized for my CPU!

Tough luck. You can still compile packages yourself just as before!

### What settings do the packages for other architectures and ABIs use?

The binary package hosting is wired up with the stage builds. Which
means, for about every stage there is a binary package hosting which 
covers (only) the stage contents and settings. There are no further plans 
to expand coverage for now. But hey, this includes the compiler (gcc or
clang) and the whole build toolchain!

### Are the packages cryptographically signed?

Yes, with the [same key as the stages](https://www.gentoo.org/downloads/signatures/).

### Are the cryptographic signatures verified before installation?

Yes, with one limitation (in the default setting).

Portage knows two binary package formats, XPAK (old) and GPKG (new). Only GPKG supports
cryptographic signing. Until recently, XPAK was the default setting (and it may still
be the default on your installation since this is not changed during upgrade, but only
at new installation).

The new, official Gentoo binary packages are all in GPKG format.
GPKG packages have their signature verified, and if this fails, installation is refused.
To avoid breaking compatibility with old binary packages, by default XPAK
packages (which do not have signatures) can still be installed however.

If you want to require verified signatures (which is something we strongly recommend), 
set `FEATURES="binpkg-request-signature"` in `make.conf`. Then, obviously, you can also
only use GPKG packages.

### I get an error that signatures cannot be verified.

Try running the Gentoo Trust Tool `getuto` as root.

> `$ getuto`

This should set up the required key ring with the Gentoo Release Engineering keys for
Portage. 

If you have `FEATURES="binpkg-request-signature"` enabled in `make.conf`, then `getuto`
is called automatically before every binary package download operation, to make sure
that key updates and revocations are imported.

### I've made binary packages myself and portage refuses to use them now!

Well, you found the side effect of `FEATURES="binpkg-request-signature"`.
For your self-made packages you will need to set up a signing key and have that key
trusted by the anchor in `/etc/portage/gnupg`. 

The [binary package guide](https://wiki.gentoo.org/wiki/Binary_package_guide) on
our Wiki will be helpful here.

### My download is slow.

Then pretty please use a [local mirror](https://www.gentoo.org/downloads/mirrors/)
instead of downloading from University of Oregon. You can just edit the URI 
in your `/etc/portage/binrepos.conf`. And yes, that's safe, because of the 
cryptographic signature.

### My Portage still wants to compile from source.

If you use useflag combinations deviating from the profile default, then
you can't and won't use the packages. Portage will happily mix and match though
and combine binary packages with locally compiled ones. Gentoo still remains
a source-based distribution, and we are not aiming for a full binary-only 
installation without any compilation at all.

### Can I use the packages on a merged-usr system?

Yes. (If anything breaks, then this is a bug and [should be reported](https://bugs.gentoo.org/).)

### Can I use the packages with other (older or newer) profile versions?

**No.** That's why the `src-uri` path contains, e.g.,  "17.1".
**When there's a new profile version, we'll also provide new, separate package directories.**

### Any plans to offer binary packages of ~amd64 ?

Not yet. This would mean a ton of rebuilds... If we offer it one day, it'll 
be at a separate URI for technical reasons.

The advice for now is to stick to stable as much as possible, and locally
add in `package.accept_keywords` whatever packages from testing you want to use.
This means you can still use a large amount of binary packages, and just
compile the rest yourself.

### I have found a problem, with portage or a specific package!

Then please **ask for advice** (on IRC, the forums, or a mailing list) and/or 
[**file a bug!**](https://bugs.gentoo.org/)

Binary package support has been tested for some time, but with many more people using
it edge cases will certainly occur, and quality bug reports are always appreciated!

### Any pretty pictures?

Of course! Here's the amount of binary package data in GByte for each architecture...

<img style="filter: invert(1);" src="https://www.akhuettel.de/~huettel/plots/mirrors/binpackages-month.png" alt="package data over time"/>
