---
title: 'Google Summer of Code 2021 students welcome'
---

We are glad to welcome Leo and Mark to the Google Summer of Code 2021.

Mark will work on improving Catalyst, our release building tool.
Leo will work on improving our Java packaging support, with a special focus on big-data and scientific software.
