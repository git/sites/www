---
title: Impact of SKS keyserver poisoning on Gentoo
---

The [SKS keyserver network has been a victim of certificate poisoning
attack](https://gist.github.com/rjhansen/67ab921ffb4084c865b3618d6955275f)
lately.  The OpenPGP verification used for repository syncing is protected
against the attack.  However, our users can be affected when using GnuPG
directly.  In this post, we would like to shortly summarize what the attack is,
what we did to protect Gentoo against it and what can you do to protect your
system.

<!--more-->

The certificate poisoning attack abuses three facts: that OpenPGP keys can
contain unlimited number of signatures, that anyone can append signatures
to any key and that there is no way to distinguish a legitimate signature
from garbage.  The attackers are appending a large number of garbage signatures
to keys stored on SKS keyservers, causing them to become very large and cause
severe performance issues in GnuPG clients that fetch them.

The attackers have poisoned the keys of a few high ranking OpenPGP people
on the SKS keyservers, including one Gentoo developer.  Furthermore, the
current expectation is that the problem won't be fixed any time soon, so it
seems plausible that more keys may be affected in the future.  We recommend
users not to fetch or refresh keys from SKS keyserver network (this includes
aliases such as `keys.gnupg.net`) for the time being.  GnuPG upstream is
already working on client-side countermeasures and they can be expected to
enter Gentoo as soon as they are released.

The Gentoo key infrastructure has not been affected by the attack.  Shortly
after it was reported, we have disabled fetching developer key updates from SKS
and today we have disabled public key upload access to prevent the keys stored
on the server from being poisoned by a malicious third party.

The gemato tool used to verify the Gentoo ebuild repository uses
[WKD](https://wiki.gnupg.org/WKD) by default. During normal operation it should
not be affected by this vulnerability. Gemato has a keyserver fallback that
might be vulnerable if WKD fails, however gemato operates in an isolated
environment that will prevent a poisoned key from causing permanent damage to
your system. In the worst case; Gentoo repository syncs will be slow or hang.

The webrsync and delta-webrsync methods also support gemato, although it is
not used by default at the moment.  In order to use it, you need to remove
`PORTAGE_GPG_DIR` from `/etc/portage/make.conf` (if it present) and put
the following values into `/etc/portage/repos.conf`:

    [gentoo]
    sync-type = webrsync
    sync-webrsync-delta = true  # false to use plain webrsync
    sync-webrsync-verify-signature = true

Afterwards, calling `emerge --sync` or `emaint sync --repo gentoo` will use
gemato key management rather than the vulnerable legacy method.  The default is
going to be changed in a future release of Portage.

When using GnuPG directly, Gentoo developer and service keys can
be securely fetched (and refreshed) via:

1. Web Key Directory, e.g. `gpg --locate-key developer@gentoo.org`
2. [Gentoo keyserver](https://keys.gentoo.org),
   e.g. `gpg --keyserver hkps://keys.gentoo.org ...`
3. Key bundles, e.g.:
   [active devs](https://qa-reports.gentoo.org/output/active-devs.gpg),
   [service keys](https://qa-reports.gentoo.org/output/service-keys.gpg)

Please note that the aforementioned services provide only keys specific
to Gentoo.  Keys belonging to other people will not be found on our keyserver.
If you are looking for them, you may try [keys.openpgp.org](
https://keys.openpgp.org/) keyserver that is not vulnerable to the attack,
at the cost of stripping all signatures and unverified UIDs.
