---
title: 'In Memory of Jonathan “avenj” Portnoy'
---

The Gentoo project mourns the loss of Jonathan Portnoy, better known amongst us as Jon, or _avenj_.

Jon was an active member of the International Gentoo community, almost since its founding in 1999.
He was still active until his last day.

His passing has struck us deeply and with disbelief.
We all remember him as a vivid and enjoyable person, easy to reach out to and energetic in all his endeavors.

On behalf of the entire Gentoo Community, all over the world, we would like to convey our deepest sympathy for his family and friends.
As per his wishes, the Gentoo Foundation has made a donation in his memory to the Perl Foundation.

Please join the community in remembering Jon on our [forums](https://forums.gentoo.org/viewtopic-t-1050304.html).
