---
title: 'Gentoo IRC presence moving to Libera Chat'
---

<a href="https://libera.chat/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2021/logo-liberachat.svg" alt="Libera.Chat logo" width="200">
</a>

The [Gentoo Council](https://wiki.gentoo.org/wiki/Project:Council) held an emergency single agenda item meeting today. At this meeting, 
we have decided to move the official IRC presence of Gentoo to the 
[Libera Chat IRC network](https://libera.chat/). We intend to have this move complete 
at latest by 13/June/2021. A full log of the meeting will be available for 
[download](https://projects.gentoo.org/council/meeting-logs/20210523.txt) soon.

At the moment it is unclear whether we will retain any presence on Freenode at all; we 
urge all users of the #gentoo channel namespace to move to [Libera Chat](https://libera.chat/)
immediately. IRC channel names will (mostly) remain identical. You will be able to recognize Gentoo
developers on Libera Chat by their IRC cloak in the usual form <i>gentoo/developer/*</i>. 
All other technical aspects will feel rather familiar to all of us as well. Detailed 
instructions for setting up various IRC clients can be found on the 
[help pages](https://libera.chat/guides) of the IRC network.
