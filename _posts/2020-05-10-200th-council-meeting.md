---
title: '200th Gentoo Council meeting'
---

<img src="/assets/img/news/2020/groupphoto.png" alt="council group photo" class="news-img-right">

Way back in 2005, the reorganization of Gentoo led to the formation of the [Gentoo Council](https://wiki.gentoo.org/wiki/Project:Council),
a steering body elected annually by the Gentoo developers. Forward 15 years, and today we
had our 200th meeting! (No earth shaking decisions were taken today though.) The logs and
summaries of all meetings can be read online on the [archive page](https://wiki.gentoo.org/wiki/Project:Council/Meeting_logs).
