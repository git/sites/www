---
title: 'Five Gentoo developers scheduled for talks at FOSDEM'
---

<a href="https://fosdem.org/2017/" class="news-img-right">
  <img src="/assets/img/news/2017/logo-fosdem.png" alt="FOSDEM 2017 logo">
</a>

As FOSDEM 2017 approaches we are happy to announce there are a total of five Gentoo developers scheduled to give talks!

Developers and their talks include:

* [Vladimir Smirnov](https://fosdem.org/2017/schedule/speaker/vladimir_smirnov/) ([civil <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:Civil)) – *[Graphite@Scale or How to store millions metrics per second](https://fosdem.org/2017/schedule/event/graphite_at_scale/)*
* [Andrew Savchenko](https://fosdem.org/2017/schedule/speaker/andrew_savchenko/) ([bircoph <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:Bircoph)) – *[Quantum computing and post-quantum cryptography](https://fosdem.org/2017/schedule/event/quantum/)*
* [Rafael Martins](https://fosdem.org/2017/schedule/speaker/rafael_martins/) ([rafaelmartins <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:RafaelMartins)) – *[Improving your virtualization development workflow with Lago](https://fosdem.org/2017/schedule/event/iaas_impyouvir/)*
* [Hanno Böck](https://fosdem.org/2017/schedule/speaker/hanno_bock/) ([hanno <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:Hanno)) – *[Is the Linux Desktop less secure than Windows 10?](https://fosdem.org/2017/schedule/event/linux_desktop_versus_windows10/)*
* [Jason A. Donenfeld](https://fosdem.org/2017/schedule/speaker/jason_a_donenfeld/) ([zx2c4 <i class="fa fa-user" aria-hidden="true"></i>](https://wiki.gentoo.org/wiki/User:Zx2c4)) – *[WireGuard: Next Generation Secure Kernel Network Tunnel](https://fosdem.org/2017/schedule/event/wireguard/)*

Only a few hours remain until the event kicks off. See you at FOSDEM!
