---
title: 'New Packages site features'
---

<img src="https://www.gentoo.org/assets/img/news/2020/gentoo-package.svg"
alt="Gentoo in a package" class="news-img-right" width="130">

Our [packages.gentoo.org](https://packages.gentoo.org) site has recently received major 
feature upgrades thanks to the continued efforts of Gentoo developer 
[Max Magorsch (arzano)](https://wiki.gentoo.org/wiki/User:Arzano). Highlights include:

* Tracking Gentoo bugs of [specific packages](https://packages.gentoo.org/packages/sys-apps/portage/bugs)
([Bugzilla](https://bugs.gentoo.org/) integration)
* Tracking available upstream package versions 
([Repology](https://repology.org/) integration)
* QA check warnings for specific packages ([QA reports](https://qa-reports.gentoo.org/) 
integration)

Additionally, an [experimental command-line client](https://github.com/arzano/pgo) 
for packages.gentoo.org named "pgo" is in preparation, specifically also for our users with
accesssibility needs.
