---
title: 'KDE Plasma 6 upgrade for stable Gentoo Linux'
---

<a href="https://kde.org/de/plasma-desktop/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2024/logo-plasma.png" alt="KDE Plasma logo"/>
</a>

Exciting news for stable Gentoo users: It's time for the upgrade to the new "megaversion"
of the KDE community desktop environment, <a href="https://kde.org/plasma-desktop/">KDE Plasma 
6</a>! Together with <a href="https://en.wikipedia.org/wiki/KDE_Gear">KDE Gear 24.05.2</a>, where
now most of the applications have been ported, and
<a href="https://develop.kde.org/products/frameworks/">KDE Frameworks 6.5.0</a>, the underlying
library architecture, KDE Plasma 6.1.4 will be stabilized over the next days. 
The base libraries of <a href="https://www.qt.io/product/qt6">Qt 6</a> are already available.

More technical information on the upgrade, which should be fairly seamless, as well as
architecture-specific notes can be found in a 
<a href="https://www.gentoo.org/support/news-items/2024-08-31-kde-plasma6-gear-24-05-stable.html">repository 
news item</a>. Enjoy!
