---
title: 'New Gentoo LiveGUI ISO and artwork / branding contest!'
---

<a href="https://www.gentoo.org/downloads/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2022/inked-larry.png" alt="Artist Larry" width="150"/>
</a>

After a long break, we now have again a [weekly LiveGUI ISO image](https://www.gentoo.org/downloads/) for 
amd64 available! The download, suitable for an USB stick or a dual-layer DVD, boots directly into 
[KDE Plasma](https://kde.org/plasma-desktop/) and comes with a ton of up-to-date software. This 
ranges from office applicactions such as LibreOffice, Inkscape, and Gimp all the way to many system administrator tools.

Now, we need your help! Let's make this the coolest and most beautiful Linux live image ever. [We're calling for submissions](https://www.gentoo.org/news/2022/04/03/livegui-artwork-contest.html) of artwork, themes, actually anything from a desktop background to a boot manager animation, on the topic of Gentoo! The winning entry will be added as default setting to the official LiveGUI images, and also be available for download and installation.

<!--more-->

## The artwork contest

### What are we looking for?

Gentoo-themed artwork and branding material to make our Gentoo LiveGUI the coolest Linux live medium ever.

* Incorporates the [Gentoo logo](https://www.gentoo.org/inside-gentoo/artwork/gentoo-logo.html) and maybe [other Gentoo design elements](https://wiki.gentoo.org/wiki/Project:Artwork/Artwork) (like Larry the Cow)
* Works for a wide range of screen resolutions etc.
* Is packaged more or less ready-to-use for our LiveGUI image
* Provides a coherent experience to the user, i.e., if it consists of different parts, these fit togehter
* Can be distributed in its entirety under the [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/)

We could for example imagine screen backgrounds, Plasma theming, maybe even a GRUB boot menu animation or a LibreOffice splash screen... Feel free to come up with more ideas.

If you base your work on freely available source material created by others, please keep track of the sources and their licenses in an accompanying readme file.

### What are we *not* looking for?

* Do not submit anything that infringes on third-party copyrights or trademarks. While a *Star Trek*-themed Gentoo desktop would be cool, Paramount might object and we wouldn't be able to distribute it. Same for *My Little Pony* or the *Simpsons*.
* Do not submit artwork falling under the not-safe-for-work (NSFW) category. We will recognize it when we see it, and we won't be able to distribute it.
* Do not submit artwork with political or religious statements. No matter how universally acceptable you think that these are, someone will be offended by them.

The artwork should be such that kids or colleagues can walk into your office and you don't have to quickly cover it up. :) Also, please think of your contribution [in terms of the Gentoo Code of Conduct](https://wiki.gentoo.org/wiki/Project:Council/Code_of_conduct).

### How to submit an entry

#### Package it up

* Package all the relevant files into a single tar archive and upload it to a webserver of your choice, *or* publish the files (e.g. on github) as a single git repository.
* Add a readme file with your name and contact e-mail address, the license of the files, sources and licenses for third-party material, and detailed installation instructions
* [File a bug](https://bugs.gentoo.org/enter_bug.cgi?product=Gentoo%20Release%20Media) for the release engineering team, component "LiveCD/DVD", with the summary starting with "Artwork 2022 contest entry", and add a link to your file.
* If you link to a git repository, please mention a tag or commit which we should use.
* By submitting your entry, you allow Gentoo to download, re-publish, and distribute your files (see also above remark about the license).

#### Deadline

* The contest ends 31/May/2022 at 23:59 UTC.
* Please keep your files online for at least one more month after that date, so we can review and copy them.

### Selection and announcement of the winner

* The jury consists of the Gentoo Council, the Release Engineering team, the Artwork team, and the Public Relations team (as of beginning of April 2022).
* The winner will be chosen by vote; depending on the amount and quality of the submissions, we may also pick a runner-up or more.
* The announcement of the winner or the winners will be made in June.

## The LiveGUI image

The LiveGUI image is first and foremost provided to show off Gentoo and give everyone a chance to test a full-fledged Gentoo installation. As such, we have a lot of typical "desktop applications" installed. Additionally, we tried to integrate as many system administration tools as possible, so you can also use it for everything from repartitioning your hard drives to repairing an installation.

Some of the software on the image:
* [KDE Plasma](https://kde.org/plasma-desktop/) as desktop environment
* Office productivity: [LibreOffice](https://www.libreoffice.org/), LyX, TeXstudio, XournalPP, kile
* Web browsers: [Firefox](https://www.mozilla.org/en-US/firefox/new/), [Chromium](https://www.chromium.org/Home/)
* IRC and similar: irssi, weechat
* Editors: Emacs, vim, kate, nano, joe
* Development and source control: git, subversion, gcc, Python, Perl
* Graphics: [Inkscape](https://inkscape.org/), [Gimp](https://www.gimp.org/), [Povray](http://www.povray.org/), Luminance HDR, [Digikam](https://www.digikam.org/)
* Video: [KDEnlive](https://kdenlive.org/en/)
* Disk management: hddtemp, testdisk, hdparm, nvme-cli, gparted, partimage, btrfs-progs, ddrescue, dosfstools, e2fsprogs, zfs
* Network tools and daemons: nmap, tcpdump, traceroute, minicom, pptpclient, bind-tools, cifs-utils, nfs-utils, ftp, chrony, ntp, openssh, rdesktop, openfortivpn, openvpn, tor
* Backup: mt-st, fsarchiver
* Benchmarks: bonnie, bonnie++, dbench, iozone, stress, tiobench
* ...

The list of targeted packages (corresponding to a world file) can be found in the [catalyst specification file](https://gitweb.gentoo.org/proj/releng.git/tree/releases/specs/amd64/livegui/livegui-stage1.spec); we install the newest stable version in the Gentoo repository.

In addition, since - as in a normal Gentoo installation - compiler and development tools are available, you can temporarily install more software. Just run ``emerge --sync`` and then install whatever you need (though it will be kept in memory and be gone after the next reboot).

Feedback and of course [bug reports](https://bugs.gentoo.org/enter_bug.cgi?product=Gentoo%20Release%20Media) are welcome! Enjoy!
