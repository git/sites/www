---
title: 'Gentoo booth at the FrOSCon, St. Augustin, Germany'
---

<a href="https://www.froscon.de/en/" class="news-img-right">
  <img src="/assets/img/news/2017/logo-froscon.png" alt="FroSCon logo">
</a>

As last year, there will be a Gentoo booth again at the
upcoming [FrOSCon "Free and Open Source
Conference"](https://www.froscon.de/en/) in St. Augustin near Bonn! Visitors
can meet Gentoo developers to ask any question, get Gentoo swag, and prepare,
configure, and compile their own Gentoo buttons.

The conference is 25th and 26th of August 2018, and there is no entry fee. See you there!
