---
title: 'Reviving Gentoo Bugday'
---

<a href="https://wiki.gentoo.org/wiki/Bugday" class="news-img-right">
  <img src="/assets/img/news/2020/bug.svg" alt="bug outline" width="200">
</a>

Reviving an old tradition, the next [Gentoo Bugday](https://wiki.gentoo.org/wiki/Bugday) will 
take place on Saturday 2020-06-06. Let's contribute to Gentoo and fix bugs!
We will focus on two topics in particular:
* Adding or improving documentation on the Gentoo wiki
* Fixing packages that fail with -fno-common ([bug #705764](https://bugs.gentoo.org/705764))

Join us on channel #gentoo-bugday, freenode IRC, for real-time help. See you on 2020-06-06!
