---
title: 'Distribution kernel for Gentoo'
---

<img src="https://www.gentoo.org/assets/img/news/2020/larry-cowboy.svg" alt="Larry with Tux as cowboy" class="news-img-right" width="150">

The [Gentoo Distribution Kernel project](https://wiki.gentoo.org/wiki/Project:Distribution_Kernel) is excited 
to announce that our new Linux Kernel packages are ready for a wide audience! The project aims to create a 
better Linux Kernel maintenance experience by providing ebuilds that can be used to [configure, compile, and 
install a kernel entirely through the package manager](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Kernel#Alternative:_Using_distribution_kernels) 
as well as prebuilt binary kernels. We are currently shipping three kernel packages:

* [sys-kernel/gentoo-kernel](https://packages.gentoo.org/packages/sys-kernel/gentoo-kernel) - providing a kernel with [genpatches](https://dev.gentoo.org/~mpagano/genpatches/) applied, built using the package manager with either a distribution default or a custom configuration
* [sys-kernel/gentoo-kernel-bin](https://packages.gentoo.org/packages/sys-kernel/gentoo-kernel-bin) - prebuilt version of gentoo-kernel, saving time on compiling
* [sys-kernel/vanilla-kernel](https://packages.gentoo.org/packages/sys-kernel/vanilla-kernel) - providing a vanilla (unmodified) upstream kernel

All the packages install the kernel as part of the package installation process — just like the rest of your system!
More information can be found in the [Gentoo Handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Kernel#Alternative:_Using_distribution_kernels) 
and on the [Distribution Kernel project page](https://wiki.gentoo.org/wiki/Project:Distribution_Kernel). Happy hacking!
