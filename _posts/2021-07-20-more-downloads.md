---
title: 'Additional stage downloads for amd64, ppc, x86, arm available'
---

<a href="https://www.gentoo.org/downloads/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/logo/gentoo-signet.svg" alt="Gentoo logo" width="80"/>
</a>

Following some technical reorganization and the introduction of new hardware, 
the [Gentoo Release Engineering team](https://wiki.gentoo.org/wiki/Project:RelEng) is happy to offer 
a much-expanded set of [stage files for download](https://www.gentoo.org/downloads/). Highlights are 
in particular the inclusion of [musl](https://musl.libc.org/)-based stages and of <em>POWER9</em>-optimized 
<em>ppc64</em> downloads, as well as additional [systemd](https://www.freedesktop.org/wiki/Software/systemd/)-based 
variants for many architectures.

<!--more-->
For <em>amd64</em>, [Hardened](https://wiki.gentoo.org/wiki/Project:Hardened)/SELinux stages are now 
available directly from the download page, as are stages based on the lightweight C standard library 
[musl](https://musl.libc.org/). Note that [musl](https://musl.libc.org/) requires using the musl overlay,
as described on the [page of the Hardened musl project](https://wiki.gentoo.org/wiki/Project:Hardened_musl).

For <em>ppc</em>, little-endian stages optimized for the <em>POWER9</em> CPU series have been added, as have
been big- and little-endian Hardened musl downloads.

Additionally, for all of <em>amd64</em>, <em>ppc64</em>, <em>x86</em>, and <em>arm</em>, stages are now 
available in both an [OpenRC](https://wiki.gentoo.org/wiki/OpenRC) and a 
[systemd](https://www.freedesktop.org/wiki/Software/systemd/) init system / service manager variant 
wherever that makes sense.

This all has become possible via the introduction of new build hosts. The <em>amd64</em>, <em>x86</em> 
(natively), <em>arm</em> (via [QEMU](https://www.qemu.org/)), and <em>riscv</em> (via 
[QEMU](https://www.qemu.org/)) archives are built on 
an AMD Ryzen™ 7 3700X 8-core machine with 64GByte of RAM, located in [Hetzner](https://www.hetzner.com/)'s 
Helsinki datacentre. The <em>ppc</em>, <em>ppc64</em>, and <em>ppc64le</em> / <em>power9le</em> builds are handled
by two 16-core POWER9 machines with 32GByte of RAM, provided by 
[OSUOSL POWER Development Hosting](https://osuosl.org/services/powerdev/).

Further, at the moment an <em>arm64</em> (aka aarch64) machine with an 80-core Ampere Altra CPU and 256GByte of
RAM, provided by [Equinix](https://www.equinix.com/) through 
the [Works On Arm program](https://www.worksonarm.com/), is being prepared for improved native <em>arm64</em> and 
<em>arm</em> support, so expect updates there soon!