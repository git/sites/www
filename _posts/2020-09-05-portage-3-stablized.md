---
title: 'Portage 3.0 stabilized'
---

<img src="https://www.gentoo.org/assets/img/news/2020/skating-larry-mask.svg" alt="Skating Larry" class="news-img-right" width="170">

We have good news! Gentoo's [Portage project](https://wiki.gentoo.org/wiki/Project:Portage) has recently stabilized version 3.0 of the package manager.

What's new? Well, this third version of Portage **_removes_** support for Python 2.7, which has been [an ongoing effort across the main Gentoo repository](https://wiki.gentoo.org/wiki/Project:Python#Python_2_end-of-life) by Gentoo's Python project during the 2020 year (see [this blog post](https://blogs.gentoo.org/mgorny/2020/08/02/why-proactively-clean-python-2-up/)). 

In addition, due to a user provided patch, updating to the latest version of Portage can vastly speed up dependency calculations by around 50-60%. We love to see our community engaging in our software! For more details, see [this Reddit post](https://www.reddit.com/r/Gentoo/comments/huczqb/portage_300_released_with_up_to_5060_faster/) from the community member who provided the patch. Stay healthy and keep cooking with Gentoo!
