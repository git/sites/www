---
title: 'Gentoo booth at the FrOSCon, St. Augustin, Germany'
---

<a href="https://www.froscon.de/en/" class="news-img-right">
  <img src="/assets/img/news/2017/logo-froscon.png" alt="FroSCon logo">
</a>

Upcoming weekend, 19-20th August 2017, there will be a Gentoo booth again at the 
[FrOSCon "Free and Open Source Conference" 12](https://www.froscon.de/en/), in 
St. Augustin near Bonn! Visitors can see Gentoo live in action, get Gentoo swag, 
and prepare, configure, and compile their own Gentoo buttons. See you there!
