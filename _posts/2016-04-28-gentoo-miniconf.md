---
title: 'Events: Gentoo Miniconf 2016'
---

[Gentoo Miniconf 2016](https://blogs.gentoo.org/miniconf-2016/) will be held in Prague, Czech Republic during the weekend of 8 and 9 October 2016.
Like last time, is hosted together with the [LinuxDays](https://www.linuxdays.cz/2016/en/) by the Faculty of Information Technology of the Czech Technical University.

**Want to participate?** The [call for papers](https://goo.gl/forms/76Srch8KRN) is open until 1 August 2016.
