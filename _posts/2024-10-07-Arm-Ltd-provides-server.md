---
title: 'Arm Ltd. provides fast Ampere Altra Max server for Gentoo'
---

<a href="https://www.arm.com/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2024/arm-logo.png" alt="Arm Ltd. logo"/>
</a>

We're very happy to announce that <a href="https://www.arm.com/">Arm Ltd.</a> and specifically its
<a href="https://www.arm.com/markets/computing-infrastructure/works-on-arm">Works on Arm</a> team
has sent us a fast <a href="https://amperecomputing.com/briefs/ampere-altra-family-product-brief">Ampere Altra Max
server</a> to support Gentoo development. With 96 Armv8.2+ 64bit cores, 256 GByte of
RAM, and 4 TByte NVMe storage, it is now hosted together with some of our other hardware at 
<a href="https://osuosl.org/">OSU Open Source Lab</a>. The machine will be a clear boost to our 
future arm64 (aarch64) and arm (32bit) support, via <a href="https://www.gentoo.org/downloads/#arm64-advanced">installation
stage builds</a> and <a href="https://distfiles.gentoo.org/releases/arm64/binpackages/23.0/">binary
packages</a>, architecture testing of Gentoo packages, as well as our close work with upstream 
projects such as GCC and glibc. Thank you!
