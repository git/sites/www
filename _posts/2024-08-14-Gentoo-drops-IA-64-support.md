---
title: 'Gentoo Linux drops IA-64 (Itanium) support'
---

<a href="https://en.wikipedia.org/wiki/Itanium" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2024/ia64.png" alt="Intel Itanium logo"/>
</a>

Following the removal of <a href="https://en.wikipedia.org/wiki/Itanium">IA-64 (Itanium)</a> support in the <a
href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=cf8e8658100d4eae80ce9b21f7a81cb024dd5057">Linux
kernel</a> and <a href="https://sourceware.org/git/?p=glibc.git;a=commit;h=460860f457e2a889785c506e8c77d4a7dff24d3e">glibc</a>,
and subsequent <a href="https://public-inbox.gentoo.org/gentoo-dev/75654daa-c5fc-45c8-a104-fae43b9ca490@gentoo.org/T/">discussions
on our mailing list</a>, as well as a <a href="https://projects.gentoo.org/council/meeting-logs/20240721-summary.txt">vote
by the Gentoo Council</a>, Gentoo will discontinue all ia64 profiles and keywords. The primary reason for this decision is the
inability of the Gentoo IA-64 team to support this architecture without kernel support, glibc support, and a functional development
box (or even a well-established emulator). In addition, there have been only very few users interested in this type of hardware.

As also announced <a href="https://www.gentoo.org/support/news-items/2024-08-07-removal-ia64.html">in a news item</a>, in one month,
i.e. in the first half of September 2024, all ia64 profiles will be removed, all ia64 keywords will be dropped from all packages, and all
IA-64 related Gentoo bugs will be closed.
