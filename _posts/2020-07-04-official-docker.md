---
title: 'Official Gentoo Docker images'
---

<a href="https://hub.docker.com/u/gentoo/"><img src="https://www.gentoo.org/assets/img/news/2020/logo-docker.png" alt="docker logo" class="news-img-right"></a>

Did you already know that we have [official Gentoo Docker images](https://hub.docker.com/u/gentoo/) available on 
[Docker Hub](https://hub.docker.com/)?! The most popular one is based on the [amd64 stage](https://hub.docker.com/r/gentoo/stage3-amd64).
Images are created automatically; you can peek at the source code for this on our [git server](https://gitweb.gentoo.org/proj/docker-images.git).
Thanks to the [Gentoo Docker project](https://wiki.gentoo.org/wiki/Project:Docker)!
