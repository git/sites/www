---
title: 'FOSDEM 2019'
---

<a href="https://fosdem.org/2019/" class="news-img-right">
  <img src="/assets/img/news/2019/logo-fosdem-wide.png" alt="FOSDEM logo">
</a>

It's FOSDEM time again! Join us at [Université libre de Bruxelles](https://fosdem.org/2019/practical/transportation/),
Campus du Solbosch, in Brussels, Belgium. This year's [FOSDEM 2019](https://fosdem.org/2019/) will 
be held on February 2nd and 3rd.

Our developers will be happy to greet all open source enthusiasts at 
our [Gentoo stand](https://fosdem.org/2019/stands/) in building K. 
Visit [this year's wiki page](https://wiki.gentoo.org/wiki/FOSDEM_2019) to see
who's coming. So far eight developers have specified their
attendance, with most likely many more on the way!
