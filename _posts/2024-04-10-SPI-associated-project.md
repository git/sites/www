---
title: 'Gentoo Linux becomes an SPI associated project'
---

<a href="https://www.spi-inc.org/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2024/logo-spi.png" alt="SPI Inc. logo"/>
</a>

As of this March, Gentoo Linux has become an Associated Project of 
<a href="https://www.spi-inc.org/">Software in the Public Interest</a>, 
see also the
<a href="https://www.spi-inc.org/corporate/resolutions/2024/2024-03-11.js.1/">formal invitation 
by the Board of Directors of SPI</a>. Software in the Public Interest (SPI) is a non-profit 
corporation founded to act as a fiscal sponsor for organizations that develop open source software
and hardware. It provides services such as accepting donations, holding funds and assets, ...
SPI qualifies for 501(c)(3) (U.S. non-profit organization) status. This means that all 
donations made to SPI and its supported projects are tax deductible for donors in the United States.
<a href="https://www.gentoo.org/news/2024/04/10/SPI-associated-project.html">Read on for more details...</a>

<!--more-->

## Questions & Answers

### Why become an SPI Associated Project?

Gentoo Linux, as a collective of software developers, is pretty good at being a Linux
distribution. However, becoming a US federal non-profit organization would increase 
the non-technical workload.

The current Gentoo Foundation has bylaws restricting its behavior to 
that of a non-profit, is a recognized non-profit only in New Mexico, but a for-profit 
entity at the US federal level. A direct conversion to a federally recognized 
non-profit would be unlikely to succeed without significant effort and cost.

Finding Gentoo Foundation trustees to take care of the non-technical work is an ongoing
challenge.  Robin Johnson (robbat2), our current Gentoo Foundation treasurer, spent a
huge amount of time and effort with getting bookkeeping and taxes in order after the prior
treasurers lost interest and retired from Gentoo.

For these reasons, Gentoo is moving the non-technical organization overhead to 
Software in the Public Interest (SPI). As noted above, SPI is already now recognized 
at US federal level as a full-fleged non-profit 501(c)(3). It also handles several 
projects of similar type and size (e.g., Arch and Debian) and as such has exactly the 
experience and background that Gentoo needs.

### What are the advantages of becoming an SPI Associated Project in detail?

Financial benefits to donors:
- tax deductions [1]

Financial benefits to Gentoo:
- matching fund programs [2]
- reduced organizational complexity
- reduced administration costs [3]
- reduced taxes [4]
- reduced fees [5]
- increased access to non-profit-only sponsorship [6]

Non-financial benefits to Gentoo:
- reduced organizational complexity, no "double-headed beast" any more
- less non-technical work required

[1] Presently, almost no donations to the Gentoo Foundation provide a tax benefit 
for donors anywhere in the world. Becoming a SPI Associated Project enables tax 
benefits for donors located in the USA. Some other countries do recognize donations 
made to non-profits in other jurisdictions and provide similar tax credits.

[2] This also depends on jurisdictions and local tax laws of the donor, and is often 
tied to tax deductions.

[3] The Gentoo Foundation currently pays $1500/year in tax preparation costs.

[4] In recent fiscal years, through careful budgetary planning on the part of the 
Treasurer and advice of tax professionals, the Gentoo Foundation has used
depreciation expenses to offset taxes owing; however, this is not a sustainable
strategy.

[5] Non-profits are eligible for reduced fees, e.g., of Paypal (savings of 0.9-1.29% 
per donation) and other services.

[6] Some sponsorship programs are only available to verified 501(c)(3) organizations

### Can I still donate to Gentoo, and how?

Yes, of course, and please do so! For the start, you can go to 
<a href="https://www.spi-inc.org/projects/gentoo/">SPI's Gentoo page</a> and scroll down to the
Paypal and Click&Pledge donation links. More information and more ways will be set up soon.
Keep in mind, donations to Gentoo via SPI are tax-deductible in the US!

In time, Gentoo will contact existing recurring donors, to aid transitions to
SPI's donation systems.

### What will happen to the Gentoo Foundation?

Our intention is to eventually transfer the existing assets to SPI and dissolve the Gentoo
Foundation. The precise steps needed on the way to this objective are still under discussion.

### Does this affect in any way the European Gentoo e.V.?

No. <a href="https://gentoo-ev.org/">Förderverein Gentoo e.V.</a> will continue to exist
independently.  It is also recognized to serve public-benefit purposes (§ 52 Fiscal Code of 
Germany), meaning that donations are tax-deductible in the E.U.

