---
title: 'January Events: Gentoo at SCALE14x and FOSDEM 2016'
teaserlink: Get all the details and a special discount code for attending SCALE.
---

The new year kicks off with two large events with Gentoo participation: The Southern California Linux Expo **SCALE14x** and **FOSDEM 2016,**
both featuring a Gentoo booth.

<!--more-->

### SCALE14x

<a href="https://www.socallinuxexpo.org/scale/14x" class="news-img-right">
  <img src="/assets/img/news/2016/logo-scale.png" alt="SCALE14x logo">
</a>

First we have the Southern California Linux Expo [SCALE](https://www.socallinuxexpo.org/scale/14x) in its 14th edition.
The [Pasadena Convention Center](https://www.socallinuxexpo.org/scale/14x/venue) will host the event this year from January 21 to January 24.

Gentoo will be present as an exhibitor, like in the previous years.

Thanks to the organizers, we can share a special promotional code for attending SCALE with our community, valid for full access passes.
Using the code **GNTOO** on the [registration page](https://reg.socallinuxexpo.org/reg6/) will get you a 50% discount.

### FOSDEM 2016

<a href="https://fosdem.org/2016/" class="news-img-right">
  <img src="/assets/img/news/2016/logo-fosdem.png" alt="FOSDEM 2016 logo">
</a>

Then, on the last weekend of January, we'll be on the other side of the pond in Brussels, Belgium where
[FOSDEM 2016](https://fosdem.org/2016/) will take place on January 30 and 31.

Located at the [Université libre de Bruxelles](https://fosdem.org/2016/practical/transportation/),
it doesn't just offer interesting talks, but also the finest Belgian beers when the sun sets. :)

This year, Gentoo will also be manning a [stand](https://fosdem.org/2016/stands/) with gadgets, swag, and LiveDVDs.

### Booth locations

We'll update this news item with more detailed information on how to find our booths at both conferences once
we have information from the organizers.
