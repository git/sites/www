---
title: Git changes & impact to Overlays hostnames
---

Changes to the Gentoo Git hosting setup may require URL changes in your checkouts:
Repositories are now only available via `git.gentoo.org` for authenticated users and `anongit.gentoo.org` for read-only traffic.

<!--more-->

As previously announced
[\[1\]](https://archives.gentoo.org/gentoo-dev-announce/message/0c0e1e88bf133a070edf7f833a83f8be)
[\[2\]](https://archives.gentoo.org/gentoo-announce/message/51cd96db6456fe7e540abc4c6dcac2bd),
and previously in the discussion of merging Overlays with Gentoo's primary SCM
hosting (CVS+Git):
The old overlays hostnames (`git.overlays.gentoo.org` and
`overlays.gentoo.org`) have now been disabled, as well as non-SSH traffic to
`git.gentoo.org`. This was a deliberate move to separate anonymous versus
authenticated Git traffic, and ensure that anonymous Git traffic can continued
to be scaled when we go ahead with switching away from CVS. Anonymous and
authenticated Git is now served by separate systems, and no anonymous Git
traffic is permitted to the authenticated Git server.

If you have anonymous Git checkouts from any of the affected hostnames, you
should switch them to using one of these new URLs:

- `https://anongit.gentoo.org/git/$REPO`
- `http://anongit.gentoo.org/git/$REPO`
- `git://anongit.gentoo.org/$REPO`

If you have authenticated Git checkouts from the same hosts, you should switch
them to this new URL:

- `git+ssh://git@git.gentoo.org/$REPO`

In either case, you can trivially update any existing checkout with:  
`git remote set-url origin git+ssh://git@git.gentoo.org/$REPO`  
(be sure to adjust the path of the repository and the name of the remote as
needed).
