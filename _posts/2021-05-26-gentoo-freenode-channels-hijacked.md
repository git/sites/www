---
title: 'Gentoo Freenode channels have been hijacked'
---

Today (2021-05-26) a large number of Gentoo channels have been hijacked
by Freenode staff, including channels that were not yet migrated
to Libera.chat.  We cannot perceive this otherwise than as an open act
of hostility and we have effectively left Freenode.

Please note that at this point the only official Gentoo IRC channels,
as well as developer accounts, can be found on [Libera Chat](https://libera.chat/).

## 2021-06-15 update

As a part of an unannounced switch to a different IRC daemon,
the Freenode staff has removed all channel and nickname registrations.
Since many Gentoo developers have left Freenode permanently and are not
interested in registering their nicknames again, this opens up further
possibilities of malicious impersonation.
