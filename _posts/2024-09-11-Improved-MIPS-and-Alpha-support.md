---
title: 'Much improved MIPS and Alpha support in Gentoo Linux'
---

<a href="https://www.gentoo.org/news/2024/09/11/Improved-MIPS-and-Alpha-support.html" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/news/2024/mips-alpha-logos.png" alt="MIPS and Alpha logos"/>
</a>

Over the last years, <a href="https://en.wikipedia.org/wiki/MIPS_architecture">MIPS</a> and
<a href="https://en.wikipedia.org/wiki/DEC_Alpha">Alpha</a> support in Gentoo has been slowing down,
mostly due to a lack of volunteers keeping these architectures alive. Not anymore however! We're happy
to announce that thanks to renewed volunteer interest both arches have returned to the forefront of
Gentoo Linux development, with a consistent dependency tree checked and enforced by our continuous integration system.
Up-to-date stage builds and the accompanying binary packages are available for both, in the case of
<a href="https://www.gentoo.org/downloads/#mips">MIPS</a> for all three ABI variants
o32, n32, and n64 and for both big and little endian, and in the case of
<a href="https://www.gentoo.org/downloads/#alpha">Alpha</a> also with a
bootable installation CD.
