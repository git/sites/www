---
title: 'Format of download file signatures has changed'
---

<a href="https://www.gentoo.org/downloads/" class="news-img-right">
  <img src="https://www.gentoo.org/assets/img/logo/gentoo-signet.svg" alt="Gentoo logo" width="80"/>
</a>

We have simplified the format of the downloadable file (i.e. stage 3 and iso image) signatures. 
Now, each of these files is accompanied by a detached GnuPG signature where the file itself is signed. 
The signing key remains unchanged; see our [web page on release media signatures](https://www.gentoo.org/downloads/signatures/)
for the fingerprints.

An unsigned DIGESTS file remains available as well.
