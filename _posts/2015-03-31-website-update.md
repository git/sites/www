---
title: Gentoo announces total website makeover (not an April Fool's joke!)
---

**Thank you for participating in Gentoo's 2015 April Fools' joke!**

Now that April 1 has passed, we shed a tear as we say goodbye CGA Web&trade; but also to our website.
Our previous website, that is, that has been with us for more than a decade.

<!--more-->

Until all contents are migrated, you can find the previous version on [wwwold.gentoo.org](https://wwwold.gentoo.org/),
please note that the contents found there are not maintained any longer.

As this is indeed a major change, we're still working out some rough edges
and would appreciate your feedback via email to [www@gentoo.org](mailto:www@gentoo.org)
or on IRC in [#gentoo-www](/get-involved/irc-channels/all-channels.html).

We hope you appreciate the new look and had a great time finding out how terrible you are at Pong and are looking forward
to seeing your reactions once again when we celebrate the launch of the new Gentoo Disk&trade; set.

As for [Alex](mailto:a3li@gentoo.org), [Robin](mailto:robbat2@gentoo.org), and all co-conspirators, thank you again for your participation!

<hr>

## Old April Fools' day announcement

Gentoo Linux today announced the launch of its new totally revamped and more
inclusive website which was built to conform to the CGA Web&trade; graphics
standards.

"Our previous website served the community superbly well for the past 10 years
but was frankly not as inclusive as we would have liked as it could not be
viewed by aspiring community members who did not have access to the latest
hardware," said a Gentoo Linux Council Member who asked not to be named.

"Dedicated community members worked all hours for many months to get the new
site ready for its launch today. We are proud of their efforts and are
convinced that the new site will be way more inclusive than ever and thereby
deepen the sense of community felt by all," they said.

"Gentoo Linux's seven-person council determined that the interests of the
community were not being served by the previous site and decided that it had to
be made more inclusive," said Web project lead Alex Legler (a3li). The new
site <s>is</s> was also available via Gopher (gopher://gopher.gentoo.org/).

"What's the use of putting millions of colours out there when so many in the
world cannot appreciate them and who, indeed, may even feel disappointed by
their less capable hardware platforms," he said.

"We accept that members in more fortunate circumstances may feel that a site
with a 16-colour palette and an optimal screen resolution of 640 x 200 pixels
is not the best fit for their needs but we urge such members to keep the
greater good in mind. The vast majority of potential new Gentoo Linux users are
still using IBM XT computers, storing their information on 5.25-inch floppy
disks and communicating via dial-up BBS," said Roy Bamford (neddyseagoon), a
Foundation trustee.

"These people will be touched and grateful that their needs are now being taken
in account and that they will be able to view the Gentoo Linux site comfortably
on whatever hardware they have available."

"The explosion of gratitude will ensure other leading firms such as Microsoft
and Apple begin to move into conformance with CGA Web&trade; and it is hoped it will
help bring knowledge to more and more informationally-disadvantaged people
every year," said Daniel Robbins (drobbins), former Gentoo founder.

Several teams participated in the early development of the new website and
would like to showcase their work:

- Games team (JavaScript Pong).
- Multimedia team (Ghostbusters Theme on 6 floppy drives).
- Net-News team (A list of Gentoo newsgroups).

### Phase II
The second phase of the project to get Gentoo Linux to a wider user base will
involve the creation of floppy disk sets containing a compact version of the
operating system and a selection of software essentials. It is estimated that
sets could be created using less than 700 disks each and sponsorship is
currently being sought. The launch of Gentoo Disk&trade; can be expected in
about a year.

Media release prepared by [A Jackson](mailto:ajackson@gentoo.org).

Editorial inquiries: [PR team](mailto:pr@gentoo.org).

Interviews, photography and screen shots available on request.
