#!/bin/bash

echo -n 'Updating USE flag information...'
wget -T 60 'https://api.gentoo.org/packages/use.json' -O _data/use.json.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/use.json.tmp _data/use.json
echo 'done.'
