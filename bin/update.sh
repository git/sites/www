#!/bin/bash
TRACE=${TRACE:=0}

[[ $TRACE -eq 1 ]] && set -x

bin/update-devaway.sh || exit 1
bin/update-downloads.sh || exit 1
bin/update-glsa.sh || exit 1
bin/update-keys.sh || exit 1
bin/update-mirrors.sh || exit 1
bin/update-packages.sh || exit 1
bin/update-planet.sh || exit 1
bin/update-use.sh || exit 1
bin/update-userinfo.sh || exit 1
bin/update-wiki.sh || exit 1
bin/update-git-submodules.sh || exit 1