#!/bin/bash

echo -n 'Updating GLSA information...'
wget -T 60 'https://security.gentoo.org/glsa/feed.yaml' -O _data/glsa.yaml.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/glsa.yaml.tmp _data/glsa.yaml
echo 'done.'
