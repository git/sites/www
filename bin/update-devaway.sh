#!/bin/bash

# TODO: One day, fix up the json to be compatible with jekyll's parser
echo -n 'Updating devaway information...'
wget -T 60 'https://dev.gentoo.org/devaway/xml/' -O _data/devaway.xml.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/devaway.xml.tmp _data/devaway.xml
echo 'done.'
