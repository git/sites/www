#!/bin/bash

# Ensure the script is in the base of the project directory.
cd "$(dirname "$0")"/.. || exit

echo -n 'Updating git submodules'

# Verify git exists at runtime or safely skip this update
if ! command -v git &> /dev/null; then
    echo "...skipped (git is not detected on the PATH)."
    exit 0
fi

# Git will iterate all modules for you, we want the master tip of the modules,
# not whatever is pinned in this repo.
if ! git submodule foreach --quiet 'git pull origin master --quiet' >/dev/null ; then
    echo '...failed.'
    exit 1
fi

echo '...done.'
exit 0
