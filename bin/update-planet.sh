#!/bin/bash

echo -n 'Updating Planet information...'
timeout 120 wget -T 60 'https://planet.gentoo.org/rss20.xml' -O _data/planet.xml.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/planet.xml.tmp _data/planet.xml
echo 'done.'
