#!/bin/bash
set -e
TRACE=${TRACE:=0}
[[ $TRACE -eq 1 ]] && set -x
IMAGE_NAME=docker.gentoo.org/sites/www
IMAGE_TAG=latest
IMAGE=${IMAGE_NAME}:${IMAGE_TAG}
IMAGE_SRC=./
declare -a DOCKER_RUN_OPTIONS DOCKER_BUILD_OPTIONS

docker_build() {
	# Always refresh the image
	# Docker caches it anyway
	docker build \
		--network=host \
		--quiet \
		-t "${IMAGE}" \
		"${DOCKER_BUILD_OPTIONS[@]}" \
		"${IMAGE_SRC}"
}

_docker_run_common() {
	_UID=$(id -u)
	_GID=$(id -g)
	docker run \
		--rm \
		-e JEKYLL_UID="${_UID}" \
		-e JEKYLL_GID="${_GID}" \
		-e JEKYLL_ROOTLESS= \
		-e CONNECTED=false \
		-e DEBUG=false \
		--volume="${PWD}:/srv/jekyll" \
		"${DOCKER_RUN_OPTIONS[@]}" \
		"$@"
}

docker_run_net_host() {
	_docker_run_common \
		--net=host \
		"${IMAGE}" \
	"$@"
}

docker_run_net_none() {
	_docker_run_common \
		--net=none \
		"${IMAGE}" \
	"$@"
}
