#!/bin/bash
# TODO: fix this handling for temp files and if one of the downloads fails but not others.

URI_BASE="http://distfiles.gentoo.org/releases"

ARCHES=(alpha amd64 arm arm64 hppa ia64 loong mips m68k ppc riscv s390 sh sparc x86)

usage() {
  cat <<EOF
Usage: update-downloads.sh

Helper script that runs on the servers to refresh the files that back the
downloads page: https://gentoo.org/downloads/
EOF
  exit 0
}

fetch() {
  wget -T 60 -q "$@"
}

# Append extra images to the specified list.
# append_list <local file> <list of remote urls to append>
append_list() {
  local list=$1 file
  shift
  for file in "$@"; do
    fetch -O - >>"${list}" "${uri_base}/${file}"
  done
}

update_amd64() {
  append_list "${boot_list}" "latest-admincd-amd64.txt"
  append_list "${boot_list}" "latest-qcow2.txt"
}

update_arm64() {
  append_list "${boot_list}" "latest-qcow2.txt"
}

update_hppa() {
  append_list "${boot_list}" latest-netboot-hppa{32,64}.txt
}

update_s390() {
  append_list "${boot_list}" latest-netboot-s390{,x}-{initramfs,kernel}.txt
}

update_x86() {
  append_list "${boot_list}" "latest-admincd-x86.txt"
}

# Update the bootable & stage lists.
update_arch() {
  local arch=$1
  local uri_base="${URI_BASE}/${arch}/autobuilds"
  local list_base="_data/downloads/${arch}"
  local boot_list="${list_base}/iso.txt"
  local stage_list="${list_base}/stage3.txt"

  mkdir -p "${list_base}"
  fetch "${uri_base}/latest-iso.txt" -O "${boot_list}"
  fetch "${uri_base}/latest-stage3.txt" -O "${stage_list}"
  if [[ $(type -t "update_${arch}") == "function" ]]; then
    update_${arch}
  fi
}

main() {
  if [[ $# -ne 0 ]]; then
    usage
  fi

  # Make sure we're in the top level dir.
  cd "$(dirname "$0")"/..

  local arch
  printf 'Updating downloads'
  for arch in "${ARCHES[@]}"; do
    printf ' %s' "${arch}"
    update_arch "${arch}"
  done
  echo '...done.'
}

main "$@"
