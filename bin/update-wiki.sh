#!/bin/bash

echo -n 'Updating wiki information...'
# Arguments in the wiki.gentoo.org link below:
# namespace=0 is the main namespace
# limit=5 is how many new pages are returned in the query; this was reduced from the default value of 50 since only the most recent 5 entries are displayed on the frontpage
wget -T 60 'https://wiki.gentoo.org/index.php?title=Special:NewPages&feed=rss&hidebots=1&hideredirs=1&limit=6&offset=&namespace=0&username=&tagfilter=' -O _data/wiki.xml.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/wiki.xml.tmp _data/wiki.xml
echo 'done.'
