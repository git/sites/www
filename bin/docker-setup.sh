#!/bin/bash
set -e
source "$(dirname "$0")"/docker-helper.inc
cd "$(dirname "$0")"/.. # Need to be in correct path
docker_build
docker_run_net_host "${@}"
