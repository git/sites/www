#!/bin/bash

echo -n 'Updating LDAP user information...'

DESTFILE=_data/userinfo.json
if [[ $(hostname -f) == *'gentoo.org'* ]] || [[ "$FORCE" == '1' ]]; then
  timeout 180 bin/userinfo-export.rb > "${DESTFILE}".tmp && mv -f "${DESTFILE}".tmp "${DESTFILE}"
  echo 'done.'
else
  echo 'skipped (not a gentoo.org box, assuming no LDAP available, set FORCE=1 to override).'
  if [[ ! -e "${DESTFILE}" ]]; then
	  echo '
		{
			"current": {},
			"retired": {},
			"system": {}
		}
	  ' | tr '\t' ' ' >"${DESTFILE}"
  fi
fi
