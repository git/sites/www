#!/bin/bash

echo -n 'Updating keyrings...'
if ! cd _data ; then
	echo "fail."
	exit 1
fi
URLS=(
	https://qa-reports.gentoo.org/output/service-keys.gpg
	https://qa-reports.gentoo.org/output/active-devs.gpg
	https://qa-reports.gentoo.org/output/all-devs.gpg
)
# this will do URL pipeline where possible
wget -T 60 -q -N "${URLS[@]}"
echo done.
