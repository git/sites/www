#!/bin/bash

echo -n 'Updating mirror information...'
wget -T 60 'https://api.gentoo.org/mirrors/distfiles.xml' -O _data/mirrors-distfiles.xml.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/mirrors-distfiles.xml.tmp _data/mirrors-distfiles.xml
wget -T 60 'https://api.gentoo.org/mirrors/rsync.xml' -O _data/mirrors-rsync.xml.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/mirrors-rsync.xml.tmp _data/mirrors-rsync.xml
echo 'done.'
