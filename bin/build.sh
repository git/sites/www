#!/bin/bash
set -e
source "$(dirname "$0")"/docker-helper.inc
# Run the actual build process, with no networking permitted.
# Incremental builds do not update indexes correctly, so we cannot use them.
# https://github.com/jekyll/jekyll/issues/3411
# https://github.com/jekyll/jekyll/issues/3747
# By including a container name, we automatically ensure that only one instance is running!
# We may need to explore extra timeouts in future

# Need to be able to build one per www & wwwtest concurrently.
name=gentoo.sites.www.build
[[ "$0" != "${0/build-wwwtest}" ]] && name=gentoo.sites.wwwtest.build
DOCKER_RUN_OPTIONS=( --name "$name" )

# TODO: sometimes this got stuck and the previous container was stopped but not
# removed despite the --rm. We need to detect that and clean it up in future.
docker_run_net_none jekyll build
