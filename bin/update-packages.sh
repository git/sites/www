#!/bin/bash

echo -n 'Updating packages information...'
timeout 60 wget -T 60 'https://packages.gentoo.org/packages/added.atom' -O _data/packages.xml.tmp 2>/dev/null
[ $? -eq 0 ] && mv _data/packages.xml.tmp _data/packages.xml
echo 'done.'
