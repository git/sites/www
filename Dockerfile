ARG JEKYLL_VERSION=4.1.0
FROM jekyll/jekyll:${JEKYLL_VERSION}
# py3-pygments: Needed for rbst Gem, because it calls Python directly
# py3-docutils: Needed for GLEPs
# GnuPG: Needed for WKD
RUN apk add --no-cache \
	gnupg \
	python3 \
	py3-docutils \
	py3-pygments \
	&& ln -sf python3 /usr/bin/python

# Ruby stuff
COPY Gemfile /srv/jekyll/
COPY Gemfile.lock /srv/jekyll/
ARG BUNDLE_JOBS=2
RUN cd /srv/jekyll/ \
	&& bundle install \
	&& rm -vrf \
		/home/jekyll/.bundle/cache
