---
title: 'Gentoo Foundation'
navtitle: 'Gentoo Foundation'
nav1: inside-gentoo
nav2-show: true
nav2: foundation
nav2-weight: 30
nav3-show: true
nav3-weight: 1
nav3: foundation-index
layout: page-nav3
body_class: nav-align-h2
---

## Why a Gentoo Foundation?

The Gentoo distribution is maturing rapidly. Since its birth in 1999 the distribution has evolved to one of the top Linux distributions available today.
The incorporated technologies including Portage, the software management system, are frequently seen as major software breakthroughs.

In order to sustain the current quality and development swiftness the Gentoo project needs a framework
for intellectual property protection and financial contributions while limiting the contributors' legal exposure.
The Gentoo Foundation will embody this framework without intervening in the Gentoo development.
This latter aspect should be seen as a clear separation between coordinating the Gentoo development and protecting Gentoo's assets.
Both are distinct concepts requiring different skills and working methods.

The Gentoo Foundation decouples necessary bureaucracy from development, providing faster and higher quality results
as the developer can now focus his attention completely at the community and his responsibilities (being software, documentation, infrastructure or others).

The bureaucracy we mention includes:

* Financial caretaking: accepting donations, managing financial assets and investing in required resources or entities that benefit the Gentoo development.
* Juridical protection: backing up the licenses Gentoo uses, maintaining the copyrights on Gentoo's software, documentation and other assets and protecting Gentoo's intellectual property.
* Gentoo caretaking: protecting the community by requiring total adherence to the Gentoo Social Contract.

In other words, the Gentoo Foundation will:

* Protect the use of the Gentoo trademark and logo.
* Protect the developed code, documentation, artwork and other material through copyright and licenses.
* Sponsor Gentoo-related conferences and technical development resources.
* Oversee development so it adheres to the social contract.

## Find out more

The Gentoo Foundation maintains a [presence on the Gentoo Wiki](https://wiki.gentoo.org/wiki/Foundation:Main_Page) where you can learn more.
