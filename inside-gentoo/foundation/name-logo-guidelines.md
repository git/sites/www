---
title: 'Gentoo name and logo usage guidelines'
navtitle: 'Name and logo guidelines'
nav1: inside-gentoo
nav2: foundation
nav3-show: true
nav3-weight: 10
nav3: foundation-name-logo
layout: page-nav3
body_class: nav-align-h2
---

## Preamble

The Gentoo Foundation, Inc. is the owner of the Gentoo trademark (for the United
States of America and other regions that recognize a U.S.A. registration), and
as such, it is necessary for us to protect this mark. Part of this protection is
to clearly define criteria that must be satisfied in order to use the Gentoo
trademark.

The Gentoo Project is a Free Software project, and as such it is important to
the Gentoo Foundation, Inc. that the uses of the Gentoo trademark are in the
spirit of Free Software. This license attempts to codify the practices generally
accepted in a Free Software community regarding the use of a trademark. Broadly
speaking, this means that the community should be allowed to use the trademark
to identify related products, libraries, community organizations, events, or
education and training materials, as long as that activity doesn't violate the
spirit of Free Software. The license also attempts to place some light social
pressure on those directly using the Gentoo trademark to make money. Anyone
making a profit by using the Gentoo trademark is required to report what
proportion of that money will be contributed back to the community.

It is also important to note that this license only applies if you intend to use
(or are already using) a trademark of your own that incorporates the Gentoo name,
and/or incorporates the official logo/colors of the Gentoo project. If your usage
of the Gentoo name is purely nominative, you don't need to license the Gentoo
trademark - nominative use is an allowed use under trademark law.

## Gentoo trademark license

The Gentoo Foundation, Inc., as the owner of registered and common law trademarks
of the Gentoo name and logo (in the United States of America and other regions
recognizing its registrations), licenses the use of the Gentoo trademarks to the
community under the following conditions.

![Gentoo Logo](/assets/img/logo/gentoo-3d-small.png)

### Service identification

You may use the Gentoo name and logo on a list of services, tools, or supported
platforms, or as an identifier that a product or service is Gentoo-related,
provided that the usage of the name and/or logo does not imply that the
organization is official or otherwise endorsed by the Gentoo Foundation, Inc.,
or by the Gentoo project, and that the name and/or logo acts as a hyperlink to
the main [Gentoo project](https://www.gentoo.org) website wherever technically
possible.

### Gentoo-related software projects

You may not use the Gentoo name in any software project that can be used to
augment or extend the capabilities of official Gentoo project software (hosted
on the gentoo.org domain).

The use of the Gentoo name or logo for repositories, overlays or projects that
solely focus on the official Gentoo project software and documentation is seen
as [service identification](#service-identification) and is governed under that
section.

### Groups and events

You may incorporate the Gentoo name into the name of any group or event,
provided that:

* The purpose of the group or event is to educate, provide networking
  opportunities, or provide a social outlet for Gentoo users;
* The name of the group or event is formed by combining the name "Gentoo" with
  other words that qualify the geographical location, intended audience, or the
  activities of the group or event.
* If the group or event requires fees for membership of attendance, it must be
  made clear at the time the fee is paid what income organizers will be
  deriving from the event, and what proportion of event profits, if any, will
  be returned to the Gentoo Foundation, Inc. as a donation.
* The website for the group or event does not imply that it is official or
  otherwise endorsed by the Gentoo Foundation, Inc.
* The group or event agrees to adhere to the
  [Gentoo Community Code of Conduct](https://wiki.gentoo.org/wiki/Project:Council/Code_of_conduct).
* The group or event also adopts a specific Code of Conduct governing the
  behavior of participants.

A group or event may use a logo that incorporates the Gentoo logo by adding a
word or picture that identifies the locality (for example, the name of a city,
or a stylized image of an identifiable city landmark), audience, or activities
of the group of event.

### Merchandise

You may produce merchandise displaying the Gentoo name or logo, provided that:

* Artwork to be printed on merchandise has been formally approved by the Gentoo
  Foundation, Inc.
* The products on which the artwork is to appear has been formally approved by
  the Gentoo Foundation, Inc.
* If the product is to be sold, the individual or organization producing the
  product must make a clear statement declaring what proportion of the sale
  price will be retained by the organizers, and what proportion, if any, will be
  contributed back to the Gentoo Foundation, Inc.

To obtain formal approval for your merchandise sale, please submit an electronic
copy of the proof artwork, plus a list of all products to be sold, to
[trustees@gentoo.org](mailto:trustees@gentoo.org).

We will not approve any variations of the Gentoo logo that change the aspect
ratio or otherwise distort the shape.

### Products and services serving the community

You may incorporate the Gentoo name into any website or product whose purpose is
to organize, educate or inform the Gentoo community, provided that:

* The website or product does not use the Gentoo logo or mimic the official
  sites in its design, except as provided for by earlier sections in this
  license;
* The website or product does not imply that it is official or otherwise
  endorsed by the Gentoo Foundation, Inc.
* If a fee is to be charged for the product, or for access to any or all of the
  material on the website, a clear statement must exist declaring what
  proportion of those fees will be retained by the authors, and what proportion,
  if any, will be contributed back to the Gentoo Foundation, Inc. A link to this
  statement must be prominently displayed on the website or product.

### Other commercial activity

You may *not* use the Gentoo name in the registered name of any company that
offers Gentoo project related services.

You may *not* incorporate the Gentoo name or logo into the name of any product to
be sold by a commercial entity when that product or entity is Gentoo project
related.

### Domain names

You may use the Gentoo name in any Internet domain or subdomain:

* For any group or event meeting the requirements of the section on [Groups and
  events](#groups-and-events).
* Any merchandise provider meeting the requirements of the section on
  [Merchandise](#merchandise), provided the products sold under that domain are
  exclusively Gentoo merchandise
* Any website or product meeting the requirements of the section on [Products and
  services serving the community](#products-and-services-serving-the-community).

### Uses outside of this license

If your usage, or proposed usage, of the Gentoo name or trademark does not meet
these criteria, a specific license may be granted to your organization, at the
Gentoo Foundation, Inc. discretion. Please contact
[trustees@gentoo.org](mailto:trustees@gentoo.org) with the
details or your proposed usage, and a license may be granted.

The restrictions about using the "Gentoo" name do not apply to the [Gentoo File
Manager](https://sourceforge.net/projects/gentoo/) project, developed by Emil Bink.

### Community standards

Notwithstanding the above, licensees of the Gentoo mark must not use that mark
to cast the Gentoo project, Gentoo Foundation, Inc., or Gentoo community into
disrepute, or engage in business practices that reflect poorly on the Gentoo
project, Gentoo Foundation, Inc., or Gentoo community.

### Interpretation

The Gentoo Foundation, Inc. reserves the right to determine if a usage of the
Gentoo trademark meets the requirements and standards of this license.

## Frequently asked questions

### What is the copyright license of the logo's graphics?

The official version of the "g" logo is licensed under the
[Creative Commons Attribution/Share-Alike 4.0 License](<https://creativecommons.org/licenses/by-sa/4.0/>).
See the [Gentoo Artwork project](https://wiki.gentoo.org/wiki/Project:Artwork/Artwork#Variations_of_the_.22g.22_logo)
for details.

### What about earlier name/logo usage guidelines?

The [previous name/logo usage
guidelines](https://web.archive.org/web/20160420043020/https://www.gentoo.org/inside-gentoo/foundation/name-logo-guidelines.html)
as published by the Gentoo Foundation, Inc. remain available for those
applications granted before the publication of this new license.

### What about non-USA trademarks?

The [Lizenzierung der Marke "Gentoo"](https://gentoo-ev.org/downloads/Nutzungsbestimmungen.pdf)
as published by the Förderverein Gentoo e.V. (who also owns the trademarks in
Europe) defines the usage rights of the name and logo inside Europe.

### What is this license based on?

The new License has been based on the [Django Trademark License Agreement](https://www.djangoproject.com/trademarks/)
and is, as such, also released under the [Creative Commons Attribution-ShareAlike 3.0 License](http://creativecommons.org/licenses/by-sa/3.0/).
