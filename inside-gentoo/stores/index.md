---
title: 'Stores offering Gentoo products'
navtitle: 'Stores'
nav1: inside-gentoo
nav2: stores
nav2-show: true
nav2-weight: 50
---

This page lists web stores where you can get Gentoo swag.

## Approved vendors

Gentoo merchanise may be purchased from the following vendors who have been licensed to use the
Gentoo Logo by either the [Gentoo Foundation Inc](https://foundation.gentoo.org) (in the USA) or
[Friends of Gentoo e.V.](https://www.gentoo-ev.org) within the European Union.

Approved Vendors make a small contribution to Gentoo from sales of Gentoo related merchandise.

### Americas

* [Offical Gentoo Cafepress store](https://store.gentoo.org/) - Clothing and schwag. Located in the USA.
* [Techiant, LLC](https://www.case-badges.com/gentoo-logo-3d-domed-computer-case-badges-p-212.html) - Case badges (stickers). Located in the USA

### Europe

* [Offical Friends of Gentoo e.V. store](https://shop.spreadshirt.de/22258/) - Tee shirts, keychains, and mugs. Located in Germany.
* Evelin Logo's ([Etsy](https://www.etsy.com/shop/BadgeShopArt), [eBay](https://www.ebay.com/str/5starbadgeshop), [Amazon](https://www.amazon.com/s?me=A2572FU4YZ28PJ&marketplaceID=ATVPDKIKX0DER)) - Badges (stickers). Located in Romania.
* [FreeWear.org](https://www.freewear.org/Gentoo) - Clothing, mugs, and stickers. Located in Spain.

If you buy something from a licenced vendor and you like it, tell all your friends.

If you have problems with quality or the vendor, please [tell us](mailto:trustees@gentoo.org) and the vendor.
We do not undertake to resolve individual issues but we take the quality of goods bearing our logo very seriously indeed.

## Unlicensed vendors

If you would like a listing here, please email the [trustees](mailto:trustees@gentoo.org).
A listing on this page is included in the [license](/inside-gentoo/foundation/name-logo-guidelines.html).
Gentoo must defend its registered marks to prevent them becoming public domain and we try to do it in the friendy way that is the hallmark of the open source community.

### Want to be listed on this page?

For vendor listing on this page, Gentoo requires that you sell our media at no "profit" as described by the GPL (reasonable cost to cover expenses)
or make an agreement with Gentoo for the listing.
That agreement could be one where you donate some amount to Gentoo or could be free, at Gentoo's discretion.
Vendors supporting open source will be considered heavily for free listing.
We do not list commercial vendors who do not give back to Gentoo because we already have a Gentoo store.
If you are making a profit, contact the Gentoo foundation to negotiate an agreement.
