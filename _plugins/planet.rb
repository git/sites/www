module Gentoo
  class PlanetGenerator < Jekyll::Generator
    PLANET_XML = '_data/planet.xml'

    def generate(site)
      planetinfo = Nokogiri::XML(File.open(PLANET_XML))
      # author is the only thing taken from the dublin core, we don't need namespaces for that
      planetinfo.remove_namespaces!

      site.data['planet'] ||= { 'posts' => [] }

      planetinfo.xpath('/rss/channel/item').each do |item|
        item_data = {}
        ignore = false

        item.children.each do |tag|
          case tag.name
          when 'title'
            item_data['title'] = tag.text.strip
          when 'link'
            item_data['uri'] = tag.text
          when 'creator'
            if tag.text =~ /^(.*) \(([^)]+)\)$/
              item_data['author'] = $1
              item_data['nick'] = $2
            else
              # It's a news item; skip
              ignore = true
            end
          end
        end

        site.data['planet']['posts'] << item_data unless ignore
      end
    end
  end
end
