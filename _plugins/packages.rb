module Gentoo
  class PackagesGenerator < Jekyll::Generator
    XML = '_data/packages.xml'

    def generate(site)
      generate_soko(site)
    end

    def generate_soko(site)
      xml = nil
      begin
        xml = Nokogiri::XML(File.open(XML))
        # atom is mainly one namespace
        xml.remove_namespaces!
      rescue
        # Bail out if it's not valid XML
        return []
      end

      site.data['packages'] ||= { 'updates' => [] }

      xml.xpath('/feed/entry').each do |item|
        item_data = {}

        entry_by_tagname = {}
        item.children.each do |tag|
          entry_by_tagname[tag.name] = tag
        end

        # Have to split out CAT/PN from CPV, the easiest place is the id
        # element.
        if entry_by_tagname['id'].text =~ /^tag:packages.gentoo.org,(\d{4}-\d{2}-\d{2}):\/package\/(.*)/ then
          item_data['date'] = $1
          item_data['atom'] = item_data['atom_cpv'] = $2
          item_data['atom_c'], item_data['atom_pn'] = item_data['atom'].split('/', 2)
          item_data['atom_pv'] = nil
          if entry_by_tagname['title'].text.start_with?(item_data['atom']) then
            item_data['atom_pv'] = entry_by_tagname['title'].text.delete_prefix(item_data['atom']+"-")
          end
          item_data['description'] = entry_by_tagname['summary'].text
          item_data['uri'] = entry_by_tagname['link']['href']
          if item_data['atom_pv'] then
            item_data['atom_cpv'] = sprintf('%s/%s-%s',
                                            item_data['atom_c'],
                                            item_data['atom_pn'],
                                            item_data['atom_pv'])
          end

          site.data['packages']['updates'] << item_data
        end
      end
    end

    def generate_kkuleomi(site)
      xml = Nokogiri::XML(File.open(XML))
      # atom is mainly one namespace
      xml.remove_namespaces!

      site.data['packages'] ||= { 'updates' => [] }

      xml.xpath('/feed/entry').each do |item|
        item_data = {}

        item.children.each do |tag|
          case tag.name
          when 'title'
            if tag.text =~ /^([^(]*) \((.*)\)$/
              item_data['atom'] = $1.strip
              item_data['atom_c'], item_data['atom_p'] = item_data['atom'].split('/', 2)
              item_data['description'] = $2
            end
          when 'link'
            item_data['uri'] = tag['href']
          end
        end

        site.data['packages']['updates'] << item_data
      end
    end
  end
end

# Example from kkuleomi
#  <entry>
#    <id>tag:packages.gentoo.org,2015-10-03:added:dev-java/ant-apache-regexp</id>
#    <published>2020-06-23T16:21:26Z</published>
#    <link rel="alternate" type="text/html" href="https://packages.gentoo.org/packages/dev-java/ant-apache-regexp"/>
#    <updated>2020-06-23T16:21:26+00:00</updated>
#    <title>dev-java/ant-apache-regexp (Apache Ant's optional tasks depending on jakarta-regexp-1.4)</title>
#    <content>dev-java/ant-apache-regexp is now available in Gentoo on these architectures: amd64, ppc64, x86, ~amd64-linux, ~x86-linux, ~ppc-macos, ~x64-macos, ~x86-macos, ~sparc-solaris, ~sparc64-solaris, ~x64-solaris, ~x86-solaris</content>
#  </entry>
#
# Example from Soko:
#  <entry>
#    <title>dev-python/requests-unixsocket-0.2.0</title>
#    <updated>2020-06-21T10:16:38Z</updated>
#    <id>tag:packages.gentoo.org,2020-06-21:/package/dev-python/requests-unixsocket</id>
#    <content type="html">dev-python/requests-unixsocket-0.2.0 is now available in Gentoo on these architectures: ~amd64 ~x86. See &lt;a href=&#39;https://gitweb.gentoo.org/repo/gentoo.git/commit/?id=cc81527c2dd329d517f9439123d52197968865d1&#39;&gt;Gitweb&lt;/a&gt;</content>
#    <link href="https://packages.gentoo.org/package/dev-python/requests-unixsocket" rel="alternate"></link>
#    <summary type="html">Use requests to talk HTTP via a UNIX domain socket</summary>
#    <author>
#      <name>Michał Górny</name>
#    </author>
#  </entry>
