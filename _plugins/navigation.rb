module Gentoo
  module Tags
    class NavigationTag < Liquid::Tag
      def initialize(tag_name, level, tokens)
        super
        @level = level.strip
      end

      def render(context)
        all_pages = context.registers[:site].pages
        current_page = context.registers[:page]

        if @level == 'primary'
          primary(all_pages, current_page)
        elsif @level == 'secondary'
          secondary(all_pages, current_page)
        elsif @level == 'tertiary'
          tertiary(all_pages, current_page)
        elsif @level =~ /^sitemap:(.*)$/
          sitemap(all_pages, $1)
        else
          'unknown navigation level'
        end
      end

      private

      def primary(all_pages, current_page)
        pages = all_pages.select { |page| page.data['nav1-show'] == true }

        generate(pages, current_page, '1')
      end

      def secondary(all_pages, current_page)
        pages = all_pages.select { |page| page.data['nav1'] == current_page['nav1'] && page.data['nav2-show'] == true }

        generate(pages, current_page, '2')
      end

      def tertiary(all_pages, current_page)
        pages = all_pages.select do |page|
          page.data['nav1'] == current_page['nav1'] &&
          page.data['nav2'] == current_page['nav2'] &&
          page.data['nav3-show'] == true
        end

        generate(pages, current_page, '3')
      end

      def sitemap(all_pages, nav1)
        pages = all_pages.select { |page| page.data['nav1'] == nav1 && page.data['nav2-show'] == true }

        generate(pages, {}, '2')
      end

      def generate(all_pages, current_page, level)
        level_show = 'nav%s-show' % level
        level_weight = 'nav%s-weight' % level
        level_str = 'nav%s' % level

        pages = all_pages.select { |page| page.data[level_show] == true }
        pages.sort! do |a, b|
          if a.data.key?(level_weight) && b.data.key?(level_weight)
            a.data[level_weight] <=> b.data[level_weight]
          else
            a_title = a.data['navtitle'] || a.data['title']
            b_title = b.data['navtitle'] || b.data['title']

            a_title <=> b_title
          end
        end

        html = ''
        pages.each do |page|
          css_class = current_page[level_str] == page.data[level_str] ? 'active' : ''

          html += "<li class=\"%s\">" % css_class

          if page.data.key? 'redirect'
            html += '<a href="' + page.data['redirect'] + '">'
          else
            html += '<a href="' + page.url.gsub('index.html', '') + '">'
          end

          if page.data.key? 'navtitle'
            html += page.data['navtitle']
          else
            html += page.data['title']
          end

          if page.data.key? 'redirect'
            html += ' <span class="fa fa-fw fa-external-link-square external-link" title="This link will leave www.gentoo.org."></span>'
          end

          html += "</a></li>\n"
        end

        html
      end
    end
  end
end

Liquid::Template.register_tag('navigation', Gentoo::Tags::NavigationTag)
