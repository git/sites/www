require_relative 'lib/weightedrandomizer'
require 'time'

module Gentoo
  module Tags
    class AdsTag < Liquid::Tag
      # When changing this, you must recalculate the columns in the HTML below as well!
      AD_COUNT = 4

      def render(context)
        ads = context.registers[:site].data['ads']['active']

        ad_html = ''
        first_ad = true
        raw_weighted_ads = Hash[ads.map { |ad| [ad, ad['weight'] || 0] }]
        ads_wr = WeightedRandomizer.new(raw_weighted_ads)
        ads_wr.sample(AD_COUNT * 10).uniq.slice(0, AD_COUNT).each do |ad|
          if first_ad
            ad_html += '<div class="col-xs-12 col-md-2 col-md-offset-2 sponsorlogo">'
            first_ad = false
          else
            ad_html += '<div class="col-xs-12 col-md-2 sponsorlogo">'
          end

          ad_html += '<!-- sponsor{name:%s,weight:%d} -->' % [ad['name'], ad['weight']]

          if ad.has_key? 'img'
            ad_html += "<a href=\"%s\" title=\"%s\"><img src=\"/assets/img/sponsors/ads/%s\" alt=\"%s\"></a>" %
              [ad['link'], ad['alt'], ad['img'], ad['alt']]
          else
            ad_html += "<span class=\"text-ad\"><span class=\"text-ad-content\">%s</span></span>" % ad['blurb']
          end

          ad_html += "</div>\n"
        end

        ad_html
      end
    end

    class VersionTag < Liquid::Tag
      def render(context)
        `git rev-parse --short HEAD`
      end
    end
    
    class CurrentYearTag < Liquid::Tag
      def render(context)
        Time.now.utc.year.to_s
      end
    end

    class GenerationTimestampTag < Liquid::Tag
      def render(context)
        Time.now.utc.iso8601
      end
    end


end
end

Liquid::Template.register_tag('render_ads', Gentoo::Tags::AdsTag)
Liquid::Template.register_tag('render_version', Gentoo::Tags::VersionTag)
Liquid::Template.register_tag('render_current_year', Gentoo::Tags::CurrentYearTag)
Liquid::Template.register_tag('render_generation_timestamp', Gentoo::Tags::GenerationTimestampTag)

