require 'rbst'

module Gentoo
  class GLEPGenerator < Jekyll::Generator
    GLEP_DIR = 'glep/'

    def generate(site)
      site.data['gleps'] ||= []

      Dir.chdir(GLEP_DIR) do
        Dir.glob('glep-[0-9][0-9][0-9][0-9].rst').reverse_each do |name|
          begin
            site.pages << GLEPPage.new(site, name)
          rescue
            # fail them silently
          end
        end
      end

      site.data['gleps'] = site.data['gleps'].sort { |a, b| a['GLEP'] <=> b['GLEP'] }
    end
  end

  # based on http://greyblake.com/blog/2014/10/05/lazy-object-pattern-in-ruby/
  class LazyRbST < ::BasicObject
    def initialize(rst)
      @rst = rst
    end

    def __target_object__
      @__target_object__ ||= ::RbST.new(".. contents::\n..\n\n" + @rst).to_html(
        'initial-header-level' => 2,
        'table-style' => '"table table-bordered"')
    end

    def method_missing(method_name, *args, &block)
      __target_object__.send(method_name, *args, &block)
    end
  end

  class GLEPPage < Jekyll::Page
    def initialize(site, name)
      @site = site
      @base = @site.source
      @dir = GLEPGenerator::GLEP_DIR
      @name = name

      process(@name)
      read_yaml(File.join(@base, GLEPGenerator::GLEP_DIR), name)

      data['title'] = "GLEP #{data['GLEP']}: #{data['Title']}"
      data['permalink'] = "/glep/#{@name.chomp('.rst')}.html"
      data['source'] = name
      data['layout'] = 'glep'
      data['nav1'] = 'inside-gentoo'
      data['nav2'] = 'gleps'
      data['extracss'] = ['glep.css']

      # split into lists
      ['Requires', 'Replaces', 'Replaced-By'].each do |k|
        if data.has_key?(k) then
          data[k] = data[k].to_s.split(',').map do |n|
            n.strip!
            [n, "glep-#{n.rjust(4, '0')}.html"]
          end
        end
      end

      site.data['gleps'] << data.merge({
        'url' => data['permalink']
      })

      @content = LazyRbST.new(@content)
    end

    def output_ext
      '.html'
    end

    def destination(dest)
      return super.sub('.rst', '')
    end
  end
end
