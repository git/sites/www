module Gentoo
  class NewsGenerator < Jekyll::Generator
    NEWS_DIR = '_data/news/'

    def generate(site)
      site.data['newsitems'] ||= []

      Dir.chdir(NEWS_DIR) do
        Dir.glob('[0-9][0-9][0-9][0-9]*').reverse_each do |path|
          begin
            name = path

            site.pages << NewsPage.new(site, path, name)
          rescue
            # fail them silently
          end
        end
      end

      site.data['newsitems'] = site.data['newsitems'].sort { |a, b| b['date'] <=> a['date'] }
    end
  end

  class NewsPage < Jekyll::Page
    def initialize(site, path, name)
      @site = site
      @base = @site.source
      @dir = NewsGenerator::NEWS_DIR
      @name = "#{name}.html"

      process(@name)
      read_yaml(File.join(@base, NewsGenerator::NEWS_DIR, path), "#{name}.en.txt")

      data['permalink'] = "/support/news-items/#{name}.html"
      data['layout'] = 'page-pre'
      data['nav1'] = 'support'
      data['nav2'] = 'news-items'

      File.readlines(File.join(@base, NewsGenerator::NEWS_DIR, path, "#{name}.en.txt")).each do |line|
        @title = $1 if line =~ /Title: (.*)$/
        @date = $1 if line =~ /Posted: (.*)$/
      end

      site.data['newsitems'] << { 'title' => @title, 'date' => @date, 'url' => data['permalink'] }

      data['title'] = @title
    end
  end
end
