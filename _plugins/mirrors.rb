require 'yaml'
module Gentoo
  class StaticMirrorDataGenerator < Jekyll::Generator
    DISTFILES_XML = '_data/mirrors-distfiles.xml'
    RSYNC_XML    = '_data/mirrors-rsync.xml'
    ISO3166 = '_data/iso3166-sort-of.yaml'

    def generate(site)
      site.data['mirrors'] ||= { 'rsync' => {}, 'distfiles' => {} }

      load_countries(site, ISO3166)
      load_mirrors(site, DISTFILES_XML, 'distfiles')
      load_mirrors(site, RSYNC_XML, 'rsync')
    end

    def load_countries(site, yaml_file)
      countryinfo = YAML.load(File.read(yaml_file))
      site.data['countries'] = countryinfo
    end

    def load_mirrors(site, xml, key)
      mirrorinfo = Nokogiri::XML(File.open(xml))

      mirrorinfo.xpath('/mirrors/mirrorgroup').each do |mirrorgroup|
        region = mirrorgroup['region']
        country_code = mirrorgroup['country']
        country_name = site.data&.dig('countries', country_code, 'country-name') || "Unknown-country-name:"+country_code

        site.data['mirrors'][key][region] ||= {}
        site.data['mirrors'][key][region][country_code] ||= { 'name' => country_name, 'mirrors' => [] }

        mirrorgroup.children.each do |mirror|
          mirror_data = { 'uris' => [] }
          next unless mirror.name == 'mirror'

          mirror.children.each do |tag|
            case tag.name
            when 'name'
              mirror_data['name'] = tag.text
            when 'uri'
              uri = {
                'protocol' => tag['protocol'],
                'ipv4' => tag['ipv4'],
                'ipv6' => tag['ipv6'],
                'partial' => tag['partial'],
                'uri' => tag.text
              }

              mirror_data['uris'] << uri
            end
          end

          site.data['mirrors'][key][region][country_code]['mirrors'] << mirror_data
        end
      end
    end
  end
end
