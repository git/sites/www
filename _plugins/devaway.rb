require 'date'

module Gentoo
  class DevawayGenerator < Jekyll::Generator
    DEVAWAY_XML = '_data/devaway.xml'
    # ... @ yyyy/mm/dd HH:MMZ
    DATE_REGEXP = /@ (\d+)\/(\d+)\/(\d+) (\d+):(\d+)Z$/

    def generate(site)
      data = Nokogiri::XML(File.open(DEVAWAY_XML))

      site.data['devaway'] ||= {}
      now = Time.now

      data.xpath('/devaway/dev').each do |dev|
        reason = dev.xpath('./reason/text()').first
        if reason.nil?
          puts dev['nick'] + " has an empty devaway!"
          next
        end

        reason = reason.content

        date_match = DATE_REGEXP.match(reason)
        if not date_match.nil?
          reason = date_match.pre_match.rstrip()
        end
        date = DateTime.parse(dev['timestamp'])
        away_sec = now - date.to_time

        site.data['devaway'][dev['nick']] = {
          'reason' => reason,
          'date' => date,
          'away_days' => away_sec / 24 / 3600,
        }
      end
    end
  end
end
