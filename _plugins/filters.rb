module Gentoo
  module Filters
    UNITS = %w(B KiB MiB GiB TiB).freeze

    def nice_filesize(input)
      number = input.to_i
      if number < 1024
        exponent = 0
      else
        max_exp  = UNITS.size - 1

        exponent = (Math.log(number) / Math.log(1024)).to_i
        exponent = max_exp if exponent > max_exp

        number /= 1024**exponent
      end

      "#{number} #{UNITS[exponent]}"
    end

    def rsync_url(input)
      input += '/' unless input.end_with? '/'
      input + 'gentoo-portage/'
    end
  end
end

Liquid::Template.register_filter(Gentoo::Filters)
