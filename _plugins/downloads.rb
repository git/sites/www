require 'date'

module Gentoo
  class DownloadGenerator < Jekyll::Generator
    DIR = '_data/downloads/'

    def generate(site)
      site.data['downloads'] ||= {}

      Dir.glob(DIR + '*') do |raw_arch|
        arch = File.basename(raw_arch)
        site.data['downloads'][arch] = {}

        File.readlines(raw_arch + '/iso.txt').each do |line|
          next if line.start_with? '#'
          date = nil
          if line =~ /^(\d{8}T\d{6}Z)\/(\S+) (\d+)$/
            date = Date.iso8601($1)
          elsif line =~ /^(\d{8})\/(\S+) (\d+)$/
            date = Date.parse('%s-%s-%s' % [$1[0..3], $1[4..5], $1[6..7]])
          end

          if date
            site.data['downloads'][arch]['iso'] ||= {}
            if $2.include? ('install-'+arch+'-minimal')
              site.data['downloads'][arch]['iso']['minimal'] = { 'date' => date, 'filename' => '%s/%s' % [$1, $2], 'size' => $3 }
            else
              subdir, filename, size = $1, $2, $3
              iso_name = $2.gsub(/-#{$1}.*$/, '')
              site.data['downloads'][arch]['iso'][iso_name] = { 'date' => date, 'filename' => '%s/%s' % [subdir, filename], 'size' => size }
            end
          end
        end

        File.readlines(raw_arch + '/stage3.txt').each do |line|
          next if line.start_with? '#'
          date = nil
          if line =~ /^(\d{8}T\d{6}Z)\/(\w+\/)?stage3-(.*)-\d{8}T\d{6}Z.tar.(\S+) (\d+)$/
            date = Date.iso8601($1)
          elsif line =~ /^(\d{8})\/(\w+\/)?stage3-(.*)-\d{8}.tar.(\S+) (\d+)$/
            date = Date.parse('%s-%s-%s' % [$1[0..3], $1[4..5], $1[6..7]])
          end

          if date
            site.data['downloads'][arch]['stage3'] ||= {}
            site.data['downloads'][arch]['stage3'][$3] = {
              'name' => $3,
              'date' => date,
              'filename' => '%s/%sstage3-%s-%s.tar.%s' % [$1, $2, $3, $1, $4],
              'subdir' => $2,
              'size' => $5
            }
          end
        end
      end
    end
  end
end
