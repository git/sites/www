module Gentoo
  class WikiGenerator < Jekyll::Generator
    WIKI_XML = '_data/wiki.xml'

    def generate(site)
      wikiinfo = Nokogiri::XML(File.open(WIKI_XML))
      # author is the only thing taken from the dublin core, we don't need namespaces for that
      wikiinfo.remove_namespaces!

      site.data['wiki'] ||= { 'updates' => [] }

      wikiinfo.xpath('/rss/channel/item').each do |item|
        item_data = {}

        item.children.each do |tag|
          case tag.name
          when 'title'
            item_data['title'] = tag.text
          when 'link'
            item_data['uri'] = tag.text
          when 'creator'
            item_data['author'] = tag.text
          end
        end

        site.data['wiki']['updates'] << item_data
      end
    end
  end
end
