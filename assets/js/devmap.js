/* devmap.js
 * Copyright (C) 2015 Alex Legler <a3li@gentoo.org>
 * Licensed under the terms of the Affero GPL v3.
 */

$(function() {
  L.Icon.Default.imagePath = '/assets/img/maps/';
  var map = L.map('devmap', { zoomAnimationThreshold: 9 } ).setView([30, 0], 2);
  var markers = {};

  map.addLayer(new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { minZoom: 2, maxZoom: 8, attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors' }));

  $.each(devdata, function(index, dev) {
    marker = L.marker([dev.lat, dev.lon]).addTo(map);
    marker.bindPopup("<b>" + dev.name + " (" + index + ")</b><br>" + dev.location);

    markers[index] = marker;
  });

  $('#devlist').find('a').click(function() {
    var dev = $(this).data('dev');
    $('html, body').animate({ scrollTop: $("h1").offset().top }, 500);

    setTimeout(function() {
      var location = L.latLng(devdata[dev].lat, devdata[dev].lon);
      markers[dev].openPopup();
      map.setView(location, 8, { animate: true });
    }, 500);

    return false;
  });
});