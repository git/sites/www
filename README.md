www.g.o
---

This repository is the home of www.gentoo.org.

## Building the site

### Obtaining the git submodules

Ensure the git submodules have been updated after the initial `git clone`. From the base of the project directory, run:

`git submodule update --init --recursive`

This will update the submodule files under the ''_data/news/'' and ''glep/'' directories with the latest upstream checkout.

### Using docker

#### Automagic build and run

From the root of the repository (requires app-containers/docker, app-containers/docker-cli):

```
bash ./bin/update.sh
bash bin/docker-setup.sh jekyll build 
bash bin/docker-setup.sh jekyll serve
```

#### Building and running manually 

Run the Dockerfile to build the container:

`docker build --tag "wwwgo:Dockerfile" .`

Once the container is built, run the container:

`docker container run --name wwwgo --rm --mount type=bind,source="${PWD}",target=/srv/jekyll --publish=4000:4000 --env JEKYLL_UID="$(id -u)" --env JEKYLL_GID="$(id -g)" --interactive=true --tty=true wwwgo:Dockerfile jekyll serve`

To removing all containers sourced from the wwwgo image:

`docker ps --filter "ancestor=wwwgo" --all --quiet | xargs docker container rm`

### Using bundler

Ensure dev-ruby/bundler has been emerged:

`# emerge -a dev-ruby/bundler`

From the base of the installation directory run the following to get the dependencies:

`bundler install`

Then issue the following to start the jekyll server:

`bundler exec jekyll serve`

### Building the site natively (depreciated)

This section has been depreciated due to certain ruby dependencies not being available in main Gentoo ebuild repository.

### Dependencies

To get the dependencies on Gentoo systems using Portage:

`emerge www-apps/jekyll dev-ruby/nokogiri dev-ruby/kramdown dev-ruby/rbst dev-ruby/base32`

* www-apps/jekyll - The static web engine for this site.
* dev-ruby/nokogiri - A Ruby plugin to add support for parsing HTML/XML documents. Takes action on devaway, mirror information, packages, blogs (planet), and wiki articles (see files in the _data directory).
* dev-ruby/kramdown - A Ruby plugin to add support for kramdown, a superset of the Markdown markup language. kramdown is used throughout this site.
* dev-ruby/rbst - A Ruby wrapper for processing reStructuredText via Python's Docutils. This gem is required for GLEP generation (used by _plugins/gleps.rb).
* dev-ruby/base32 - A Ruby library for base32 decoding and encoding. This gem is required for OpenPGP keys (used by _plugins/wkd.rb).

#### Optional

* net-misc/wget - Downloads files containing dynamic data (see below).

### Dynamic data

Once the dependencies have been emerged, run `bash ./bin/update.sh` from this repository's *base* directory to update the site's data that is dynamically generated. Expect "LDAP user information" to be skipped on systems that are not directly connected to Gentoo's LDAP infrastructure.

### Generation

Issue a `jekyll serve` to build the site. Once the site builds it will (by default) be hosted at http://127.0.0.1:4000

## Maintaining the site

## Posting news items

Front page news item are built from the `_posts/` directory. The simple method is to copy and paste one of the recent news posts and then edit as necessary.
