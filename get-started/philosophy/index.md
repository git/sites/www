---
nav1: get-started
nav2-show: true
nav2: philosophy
nav2-weight: 20
nav3-show: true
nav3-weight: 1
layout: page-nav3

title: 'The philosophy of Gentoo'
navtitle: 'Philosophy'
---
Every user has work they need to do. The goal of Gentoo is to design tools and systems that allow a user to do that work as pleasantly and efficiently as possible, as they see fit. Our tools should be a joy to use, and should help the user to appreciate the richness of the Linux and free software community, and the flexibility of free software. This is only possible when the tool is designed to reflect and transmit the will of the user, and leave the possibilities open as to the final form of the raw materials (the source code.) If the tool forces the user to do things a particular way, then the tool is working against, rather than for, the user. We have all experienced situations where tools seem to be imposing their respective wills on us. This is backwards, and contrary to the Gentoo philosophy.

Put another way, the Gentoo philosophy is to create better tools. When a tool is doing its job perfectly, you might not even be very aware of its presence, because it does not interfere and make its presence known, nor does it force you to interact with it when you don't want it to. The tool serves the user rather than the user serving the tool.

The goal of Gentoo is to strive to create near-ideal tools. Tools that can accommodate the needs of many different users all with divergent goals. Don't you love it when you find a tool that does exactly what you want to do? Doesn't it feel great? Our mission is to give that sensation to as many people as possible.

Daniel Robbins \\
Previous Chief Architect
