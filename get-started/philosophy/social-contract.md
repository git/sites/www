---
nav1: get-started
nav2: philosophy
nav3: social-contract
nav3-show: true
nav3-weight: 10
layout: page-nav3

title: 'Gentoo Social Contract'
navtitle: 'Social contract'
---
This social contract is intended to clearly describe the overall development policies and standards of the Gentoo project development team. Parts of this document have been derived from the [Debian Social Contract](https://www.debian.org/social_contract). Comments are welcome. Please send them to our [gentoo-project@lists.gentoo.org](mailto:gentoo-project@lists.gentoo.org) mailing list.

## What is Gentoo?

Gentoo in itself is a collection of free knowledge.
Knowledge in this context can be defined as documentation and metadata concerned with concepts or domains relevant to operating systems and their components,
as well as [free software](https://www.fsf.org/philosophy/free-sw.html) contributed by various developers to the Gentoo Project.

Gentoo, the operating system, is derived from the base concept of knowledge described above.
A Gentoo operating system should satisfy the self-hosting requirement.
In other words, the operating system should be able to build itself from scratch using the aforementioned tools and metadata.
If a product associated with an official Gentoo project does not satisfy these requirements, the product does not qualify as a Gentoo operating system.

An official list of Gentoo projects is listed under the [Gentoo metastructure](https://wiki.gentoo.org/wiki/Project:Gentoo).
A Gentoo project does not need to produce a Gentoo operating system in order to be officially recognized.

## Gentoo is and will remain free software

We will release our contributions to Gentoo as free software, metadata or documentation,
under the GNU General Public License version 2 (or later, at our discretion) or the Creative Commons Attribution-ShareAlike License version 2.0 (or later, at our discretion).
Any external contributions to Gentoo (in the form of freely-distributable sources, binaries, metadata or documentation) may be incorporated into Gentoo
provided that we are legally entitled to do so.
However, Gentoo will never depend upon a piece of software or metadata unless it conforms to the GNU General Public License, the GNU Lesser General Public License, the Creative Commons Attribution-ShareAlike License or some other license that fulfills the [Free Software Definition](https://www.gnu.org/philosophy/free-sw.html#fs-definition) or the [Definition of Free Cultural Works](https://freedomdefined.org/).

<div class="alert alert-success">
Note: The Gentoo Licenses Project maintains a list of conforming licenses in the <tt>@FREE</tt> license group, in line with the <a href="https://www.gentoo.org/glep/glep-0023.html">GLEP 23</a> specification.
</div>

## We will give back to the free software community

We will establish relationships with free software authors and collaborate with them when possible. We will submit bug-fixes, improvements, user requests, etc. to the "upstream" authors of software included in our system. We will also clearly document our contributions to Gentoo as well as any improvements or changes we make to external sources used by Gentoo (whether in the form of patches, "sed tweaks" or some other form). We acknowledge that our improvements and changes are much more meaningful to the larger free software community if they are clearly documented and explained, since not everyone has the time or ability to understand the literal changes contained in the patches or tweaks themselves.

## We will not hide problems

We will keep our [bug report database](https://bugs.gentoo.org/) open for public view at all times; reports that users file online will immediately become visible to others.

Exceptions may be made for security-related information with the request not to publicize before a certain deadline, community-relations information, or at the discretion of the Council.
