---
title: Become a developer

nav1: get-involved
nav2: become-dev
nav2-show: true
nav2-weight: 50
---
<div class="alert alert-info">
  <strong>Getting guidance:</strong>
  Your central point of contact for all questions about becoming a developer
  is the <a href="mailto:recruiters@gentoo.org" class="alert-link">Gentoo recruiters</a> team.
  Please do not hesitate to contact them!
</div>

## Becoming a developer

There are two paths for becoming a Gentoo developer. Those who would
like to focus on ebuild development will be interested in gaining commit access
to [gentoo.git](https://gitweb.gentoo.org/repo/gentoo.git) (the main ebuild repository). 
Those interested in every other area of contribution will likely still need commit access,
but not to the main ebuild repository.

<div class="alert alert-success">Ebuild work requires strict attention to technical
detail and direct collaboration between developers; those pursuing this path should 
expect a greater time commitment and additional feedback in order to develop the skills 
necessary for quality contribution.</div>

### Step 1. Contribute

If you would like to help Gentoo, first find yourself something to do. You may
want to look at the [contribution page](/get-involved/contribute/), and
subsequently look through the list of [Gentoo
projects](https://wiki.gentoo.org/wiki/Project:Gentoo). 
For ebuild work, you can try helping out one of the projects
that focus on maintaining packages, such as the
[GNOME](https://wiki.gentoo.org/wiki/Project:GNOME) or
[KDE](https://wiki.gentoo.org/wiki/Project:KDE) projects. Alternatively, you can
maintain your own packages or co-maintain other Gentoo packages via the [Proxy
Maintainers](https://wiki.gentoo.org/wiki/Project:Proxy_Maintainers)
project. For non-ebuild work, you may want to look at other projects, such as [Bug
Wranglers](https://wiki.gentoo.org/wiki/Project:Bug-wranglers), [Release
Engineering](https://wiki.gentoo.org/wiki/Project:RelEng), 
[Website Maintainers](https://wiki.gentoo.org/wiki/Project:Website),
[Wiki](https://wiki.gentoo.org/wiki/Project:Wiki),
[Public Relations](https://wiki.gentoo.org/wiki/Project:Public_Relations), 
or the [Forums](https://wiki.gentoo.org/wiki/Project:Forums).

### Step 2. Participate

If you enjoy working on Gentoo and would like to stay longer, you should
consider starting to participate in the community. You should consider joining
some of [our IRC channels](/get-involved/irc-channels/), subscribing to some of
[the mailing lists](/get-involved/mailing-lists/) and/or using [Gentoo
forums](https://forums.gentoo.org/). This way you get to know more of the Gentoo
developers and users, and they get to know you!

### Step 3. Find a mentor

The next step towards becoming a Gentoo developer is to find a mentor. A mentor
is an existing Gentoo developer who will vouch for you and help you in. Most of
the developers find their mentor among the developers they initially work with.

### Step 4. Take quizzes

Before you become an officially recognized Gentoo developer, you are expected to
learn a few basics on how Gentoo is structured and how it operates. To get commit
access, you will also need to know a fair bit about ebuilds. To help you get 
yourself knowledgeable, we have prepared one or two quizzes for you to take.

The prospective developer aiming to obtain commit access to the Gentoo ebuild repository
should take the [ebuild maintainer quiz](https://projects.gentoo.org/comrel/recruiters/quizzes/ebuild-maintainer-quiz.txt).

Developers interested in non-ebuild work will take 
the [developer quiz](https://projects.gentoo.org/comrel/recruiters/quizzes/developer-quiz.txt)
instead.

### Step 5. Get recruited!

Once you and your mentor agree that you're ready, arrange a review session
with the [Recruiters](https://wiki.gentoo.org/wiki/Project:Recruiters) at a
convenient time. During the session(s), Recruiters will interview you and ensure
that you have obtained all the knowledge necessary for success. Once you pass the review,
the Recruiters create your developer account and announce you as a new developer.


## What do I get?

Besides the warm feeling of contributing to Free and Open Source Software?

There are indeed a few perks: You get an **@gentoo.org** email address, matching IRC cloak,
special access flags on our wiki and Bugzilla sites, and a few other things to facilitate development efforts.
