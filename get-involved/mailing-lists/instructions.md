---
title: 'Mailing list instructions'
navtitle: 'Instructions and FAQs'

nav1: get-involved
nav2: ml
nav3: ml-instructions
nav3-show: true
nav3-weight: 30
layout: page-nav3
body_class: nav-align-h2
---

## Subscribing

One interacts with _mlmmj_ via email. To subscribe to a list, send an empty email to:

`listname+subscribe@lists.gentoo.org`

<div class="alert alert-info">
  <strong>Note</strong>
  <br>
  Replace <code>listname</code> with the actual name of the list that you want to subscribe to.
  <br>
  Example: <code>gentoo-user+subscribe@lists.gentoo.org</code> to subscribe to the gentoo-user mailing list.
  <br>
  You will then receive a subscription confirmation request (double opt-in) from the list manager, that you must reply to if you wish to subscribe.
</div>

## Posting

Once subscribed to the list, you can post to it by sending an email to:

`listname@lists.gentoo.org`

If you are not subscribed, your mail will be silently dropped. See the <a href="#nomail-i-subscribed-to-a-list-using-my-home-email-address-but-i-cant-post-to-the-list-from-work-what-do-i-do-to-fix-this">"Nomail"</a> section below for a further discussion.

## Unsubscribing

To unsubscribe from a list, send an empty email to:

`listname+unsubscribe@lists.gentoo.org`

<div class="alert alert-info">
  <strong>Note</strong>
  <br>
  You must use the <strong>identical address</strong> that you subscribed with to unsubscribe successfully.
  If your email address is now forwarded/rewritten beyond your control,
  please contact the list owner via <code>listname+owner@lists.gentoo.org</code> with a request for manual removal.
  You will then receive a unsubscription confirmation request (double opt-in) from the list manager, that you must reply to if you wish to be unsubscribed.
</div>

## Digest lists

All of our lists also have a corresponding digest list.
Digest lists will mail a single email to you every couple of days, rather than individual emails for each post.
If you are subscribed to the digest variant and wish to be unsubscribed, you must specifically unsubscribe from the digest variant.
Using the email address that you wish to (un)subscribe, send an empty email to the following addresses to subscribe and unsubscribe from mailing lists respectively:

`listname+subscribe-digest@lists.gentoo.org`<br>
`listname+unsubscribe-digest@lists.gentoo.org`

Some users may want to post to the list, but not actually receive mail from it (such as those who read lists via an alternate method, such as gmane).
These users may subscribe to the "nomail" option of each list:

`listname+subscribe-nomail@lists.gentoo.org`<br>
`listname+unsubscribe-nomail@lists.gentoo.org`

## More functions

You can learn more about the capabilities of mlmmj by sending an empty mail to the following address:

`listname+help@lists.gentoo.org`

## Moderated lists

Very few of the Gentoo lists are moderated: only the `-announce` lists are fully moderated.
Additionally, some of our high-traffic lists have moderators in place for spam and unsubscribe requests (matching mail via regex).
The moderators can be reached by emailing:

`listname+moderators@lists.gentoo.org`

## FAQ

### Nomail: I subscribed to a list using my home email address, but I can't post to the list from work. What do I do to fix this?

To reduce spam, all of our lists are configured to only allow posts from official subscriber email addresses.

Mail from addresses that are not subscribed is silently dropped.

It is possible to subscribe for posting-only: mlmmj supports "nomail" subscriptions, allowing you to register alternate email addresses that can be used only for posting to the list.

Here's an example of how this works:

1. Let's say you subscribed to the gentoo-dev list as `jim@home.com`, but you'd also like to post to the list using your `james@work.com` email address.
2. To do this, send a message (as `james@work.com`) to `gentoo-dev+subscribe-nomail@lists.gentoo.org`.
3. You should then be allowed to post to gentoo-dev using both your home and work email addresses.

In line with the original spam reduction goal, if you post to the list from a non-subscribed address, your mail will be delivered to `/dev/null`.

You will not receive any bounce message from our servers. This prevents spammers from forging your address to get a bounce delivered to you.

### I want to switch from regular delivery to digest delivery. How do I do this?

Unsubscribe from the normal list and then subscribe to the digest list.
For list `listname`, this would be done by sending empty emails to the following two addresses:

`listname+unsubscribe@lists.gentoo.org`<br>
`listname+subscribe-digest@lists.gentoo.org`

### How do I use procmail to filter Gentoo mailing list messages?

To filter incoming mail arriving from list `listname`, use the following <kbd>procmail</kbd> recipe:

    :0:
    * ^List-Id:.*listname\.gentoo\.org
    Mail/listname

This is identical to how you'd filter incoming _Mailman_ mailing list manager emails.

### Miscellaneous list policies

HTML email is discouraged, but not forbidden (there are some MUA, specifically web-based that make it very hard to disable HTML entirely).
Beware that some users may have their receiving side configured to hide HTML entirely, so it might look like you are ignored, especially if you sent HTML-only email.
In order of preference, you should endeavour to send: <tt>text/plain</tt>, multipart with <tt>text/plain</tt> before <tt>text/html</tt>, <tt>text/html</tt>.
MIME is acceptable and widely used.

Please do not send any vacation or out of office messages to the list.
In the interests of reducing list spam, if you set any auto-responding message that goes to the lists, we will unsubscribe your account from ALL lists.
Any previously subscribed addresses that send mail server messages to the lists will also be removed.

Do not cross-post when sending messages to a list!
Sending the same message to more than one list at a time leads to fragmentation of the thread when some people reply on one list, and some on another list.
There's no guarantee that the recipients of your message are on both lists.
Choose just one list when sending a message.

<div class="alert alert-info">
  <strong>Note</strong>
  <br>
  Note: For general email list etiquette, <a href="https://www.ietf.org/rfc/rfc1855.txt">these guidelines</a> are an excellent primer.
</div>

### My question isn't answered above, whom do I ask for help?

If you need additional help beyond the above, please [file a bug](https://bugs.gentoo.org/enter_bug.cgi?product=Gentoo%20Infrastructure&version=unspecified&component=Mailing%20Lists&rep_platform=All&op_sys=Linux&priority=P2&bug_severity=normal&bug_status=NEW&alias=&bug_file_loc=https%3A%2F%2F&short_desc=&comment=&commentprivacy=0&keywords=&dependson=&blocked=&bit-25=1&maketemplate=Remember%20values%20as%20bookmarkable%20template&form_name=enter_bug&assigned_to=infra-bugs%40gentoo.org), or contact `listname+owner@lists.gentoo.org`.

## Privacy policy and disclaimer

The mailing lists are public forums, except for lists explicitly otherwise.
For lists explicitly marked private or restricted, all of the following except web archives are applicable.

All emails sent to the lists are distributed both to the list subscribers and copied to the [Gentoo Archives](https://archives.gentoo.org),
for people to browse or search without the need to be subscribed.

There may be other places where lists are distributed — please make sure you never send any confidential or unlicensed material to the lists.
This includes things like e-mail addresses.
Of particular note is the fact that spammers, viruses, worms etc have been known to abuse e-mail addresses posted to our mailing lists.

Gentoo maintains the mailing lists in good faith and will take steps to curb all noticed abuse and maintain uninterrupted normal service.
At the same time, Gentoo is not responsible for all mailing list posts or anything that may happen in relation to them.

These policies are based on the [policies of the Debian mailing lists](https://www.debian.org/MailingLists/disclaimer).
