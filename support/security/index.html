---
title: 'Gentoo security'
navtitle: 'Security'
nav1: support
nav2: security
nav3: security-index
nav2-show: true
nav3-show: true
nav3-weight: 1
body_class: nav-align-h2

layout: page-nav3
---

<h2>Security in Gentoo Linux</h2>

<p>
  Security is a primary focus of Gentoo Linux and ensuring the confidentiality and security of our user's machines is of utmost importance to us.
  The <a href="https://wiki.gentoo.org/wiki/Project:Security">Security Project</a>
  is tasked with providing timely information about security vulnerabilities in Gentoo Linux, along with patches to secure those vulnerabilities.
  We work directly with vendors, end users, and other OSS projects to ensure all security incidents are responded to quickly and professionally.
</p>

<p>
  Documentation describing security team vulnerability treatment can be found in our <a href="vulnerability-treatment-policy.html">Vulnerability Treatment Policy</a>.
</p>

<h3>Installing a secure Gentoo system</h3>
<p>
  The <a href="https://wiki.gentoo.org/wiki/Security_Handbook">Gentoo Security Handbook</a> gives information and tips
  for building a secure system and hardening existing systems.
</p>

<h3>Keeping Gentoo secure</h3>
<p>
  Community members who wish to stay up-to-date with the security fixes should subscribe to GLSAs and apply GLSA instructions whenever an affected package is installed.
  Alternatively, regularly syncing the Gentoo ebuild repository and upgrading every package should also keep the system up-to-date security-wise.
</p>
<p>
  The <kbd>glsa-check</kbd> tool can be used to:
</p>
<ul>
  <li>Check if a specific GLSA applies to a system (<kbd>-p</kbd> option)</li>
  <li>List all GLSAs with applied/affected/unaffected status (<kbd>-l</kbd> option)</li>
  <li>Apply a given GLSA to a system (<kbd>-f</kbd> option).</li>
</ul>

<h2>Gentoo Linux Security Announcements (GLSAs)</h2>

<p>
  Gentoo Linux Security Announcements are notifications that we send out to the community to inform them of security vulnerabilities related to Gentoo Linux or the packages contained in the <a href="https://gitweb.gentoo.org/repo/gentoo.git">Gentoo ebuild repository</a>.
</p>

<h3>Recent advisories</h3>

{% include frontpage/glsa %}

<p>
 For a full list of all published GLSAs, please see our <a href="https://security.gentoo.org/glsa/">GLSA index page</a>.
</p>

<h3>How to receive GLSAs</h3>
<p>
  GLSA announcements are sent to the <a href="/get-involved/mailing-lists/">gentoo-announce@gentoo.org mailing-list</a>, and are published via <a href="https://security.gentoo.org/subscribe">RSS and Atom feeds</a>.
</p>

<h3 id="contact">Security team contact information</h3>
<p>
  Gentoo Linux takes security vulnerability reports very seriously.
  Please file new vulnerability reports on <a href="https://bugs.gentoo.org">Gentoo Bugzilla</a>
  and assign them to the <span class="emphasis">Gentoo Security</span> product and <span class="emphasis">Vulnerabilities</span> component.
  The Gentoo Linux Security Team will ensure all security-related bug reports are responded to in a timely fashion.
</p>

<p>
  If errors or omissions are found in published GLSAs, please file a bug in <a href="https://bugs.gentoo.org">Gentoo Bugzilla</a> in the <em>Gentoo Security</em> product, with the <em>GLSA Errors</em> component.
</p>

<p>
  <a href="https://bugs.gentoo.org/enter_bug.cgi?product=Gentoo%20Security&amp;component=Vulnerabilities" class="btn btn-primary btn-sm"><span class="fa fa-fw fa-bug"></span> Report security vulnerability</a>
  <a href="https://bugs.gentoo.org/enter_bug.cgi?product=Gentoo%20Security&amp;component=GLSA%20Errors" class="btn btn-primary btn-sm"><span class="fa fa-fw fa-bug"></span> Report GLSA error</a>
</p>

<h3 id="confidential-contacts">Confidential contacts</h3>
<p>
  You have two options to submit non-public vulnerabilities to the security team.
  You may submit a bug in <a href="https://bugs.gentoo.org/">Gentoo Bugzilla</a> using the <em>New-Expert</em> action, or the <em>Enter a new bug report (advanced)</em> link,
  and check the <em>Gentoo Security</em> checkbox in the <em>Only users in all of the selected groups can view this bug</em> section.
  You may also contact directly using encrypted mail one of the following security contacts:
</p>
<table class="table">
<tr>
  <td class="infohead"><b>Name</b></td>
  <td class="infohead"><b>Responsibility</b></td>
  <td class="infohead"><b>Email</b></td>
  <td class="infohead"><b>OpenPGP key ID (click to retrieve public key)</b></td>
</tr>
<tr>
  <td class="tableinfo">John Helmert III</td>
  <td class="tableinfo">Security lead</td>
  <td class="tableinfo"><a href="mailto:ajak@gentoo.org">ajak@gentoo.org</a></td>
  <td class="tableinfo"><a href="https://keys.gentoo.org/pks/lookup?op=get&search=0x39333C79B7BD85CD55C02E4C812BDFCB974B5783">0x39333C79B7BD85CD55C02E4C812BDFCB974B5783</a></td>
</tr>
<tr>
  <td class="tableinfo">Sam James</td>
  <td class="tableinfo">Security member</td>
  <td class="tableinfo"><a href="mailto:sam@gentoo.org">sam@gentoo.org</a></td>
  <td class="tableinfo"><a href="https://keys.gentoo.org/pks/lookup?op=get&search=0x5EF3A41171BB77E6110ED2D01F3D03348DB1A3E2">0x5EF3A41171BB77E6110ED2D01F3D03348DB1A3E2</a></td>
</tr>
<tr>
  <td class="tableinfo">Hans de Graaff</td>
  <td class="tableinfo">Security member</td>
  <td class="tableinfo"><a href="mailto:graaff@gentoo.org">graaff@gentoo.org</a></td>
  <td class="tableinfo"><a href="https://keys.gentoo.org/pks/lookup?op=get&search=0x818B58784EB13C5DD8CF401BBB1FE687EFDBB3EC">0x818B58784EB13C5DD8CF401BBB1FE687EFDBB3EC</a></td>
</tr>
</table>

<div class="alert alert-info">
  <strong>Note:</strong>
  In order to ensure the reception and fastest possible response for any confidential situation, we strongly encourage senders to email to at least two of the security contacts listed above.
</div>

<div class="alert alert-info">
  <strong>Note:</strong>
  A full list of Gentoo developers, including their OpenPGP key ID, is visible in our <a href="/inside-gentoo/developers/">active developers list</a>.
</div>

<h2>Resources</h2>

<h3>Security pages</h3>
<ul>
  <li><a href="https://security.gentoo.org/glsa/">GLSA index page</a> — Full list of all published GLSAs.</li>
  <li><a href="https://security.gentoo.org/glsa/feed.rss">GLSA RSS feed</a> — GLSA RSS live feed.</li>
  <li><a href="vulnerability-treatment-policy.html">Vulnerability Treatment Policy</a> — The official policy of the security team.</li>
  <li><a href="https://wiki.gentoo.org/wiki/Project:Security">Gentoo Linux Security Project</a> — The security project page.</li>
</ul>

<h3>Links</h3>
<ul>
  <li><a href="https://wiki.gentoo.org/wiki/Security_Handbook">Gentoo Security Handbook</a> — Step-by-step guide for hardening Gentoo Linux.</li>
  <li><a href="https://wiki.gentoo.org/wiki/Project:Hardened">Gentoo Hardened Project</a> — Bringing advanced security to Gentoo Linux.</li>
  <li><a href="/inside-gentoo/developers/">Active Developer List</a> — Active developer list including OpenPGP keys which can be used to verify GLSAs.</li>
</ul>
